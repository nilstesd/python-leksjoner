# Innhold

*  [Om leksjonen](#om-leksjonen)
*  [tkinter](#tkinter)
*  [Vinduer](#vinduer)
*  [Hendelser](#hendelser)
*  [StringVar](#stringvar)
*  [Widgets](#widgets)
*  [Grid](#grid)
*  [Eksempelapplikasjon](#eksempelapplikasjon)
*  [Oppgaver](#oppgaver)

# Om leksjonen

Vi skal ikke gå i dybden på grafiske brukergrensesnitt, men dere skal lære de aller mest nødvendige mekanismene og deretter skal dere få en eksempelapplikasjon som dere kan bruke som utgangspunkt for oppgaven i denne leksjonen og gjerne i prosjektet.

# tkinter

Det finnes mange forskjellige biblioteker for å lage grafiske brukergrensesnitt i Python. Vi skal bruke det som er mest standard, nemlig **tkinter**. Vi trenger ikke installere tkinter siden det allerede er en del av standard Python, men vi må importere biblioteket øverst i kodefilen hvor vi skal bruke det.

```python
import tkinter
```

# Vinduer

Nå kan vi prøve oss på å lage et eget vindu:

```python
import tkinter
root = tkinter.Tk()
root.mainloop()
```

* På første linje importerer vi biblioteket *tkinter*.
* På andre linje oppretter vi hovedvinduet i applikasjonen og lar variabelen *root* referere til dette.
* På tredje linje starter vi applikasjonen ved å si til hovedvinduet (root) at vi skal åpne vinduet og starte *event-loopen*, dvs begynne å vente på *hendelser* fra brukeren.

Når vi kjører denne applikasjonen skal vi få opp et lite vindu uten noe innhold. Vi kan avslutte applikasjonen ved å lukke vindiet eller ved å trukke *stop* i Thonny.

# Hendelser

GUI-applikasjoner er bygd opp rundt *hendelser* (event). 

Eksempler på hendelser:
* En bruker trykker på en knapp med musepekeren
* En bruker trykker på en knapp ved hjelp av tastaturtrykk
* En bruker velger et meny-element med musa eller med tastetrykk
* En bruker dobbeltklikker i en liste
* En bruker velger et element i en liste med et enkelt trykk
* En bruker endrer innholdet i et tekstfelt

En hendelse kan utløse (trigge) en aksjon. Dette er det vi som bestemmer ved at vi forteller Python hva som skal skje ved gitte hendelser.

Vi kan f.eks lage en knapp (Button) og si at funksjonen *update_data()* skal kjøres når vi trykker på den:

```python
knapp = Button(root, text="Oppdater", command=update_data)  
```

Da må denne funksjonen lages og vi må skrive koden som skal kjøres i funksjonen. Dette kan for eksempel være å kjøre et databasekall:

```python
def update_data():
    # Oppdater data i databasen
```

Vi kan lage et menyelement med en teksten *"Se statistikk"* og en funksjon, *view_statistics()*, som skal kjøres når dette velges av brukeren:

```python
menu.add_command(label="Se statistikk", command=view_statistics)
```

For knapper og menyelementer er det som regel bare en hendelse vi er interessert i, nemlig at brukeren velger dem, og derfor er det enkelt å definere hva som skal skje ved å bruke `command=?`.
I en listeboks kan det være flere typer hendelser vi er interessert i, f.eks dobbeltklikk. Da kan vi *binde opp* hendelser til aksjoner med `bind(hendelse, funksjon)`. I eksempelet under sier vi at funksjonen *edit_person()* skal kjøres når vi dobbeltklikkier på et element i listeboksen. Dette eksempelet finner du også i eksempelapplikasjonen under.

```python
result_listbox.bind('<Double-Button-1>', edit_person)
```

# StringVar

Dette er spesielle typer variabler som brukes sammen med *widgets*. Fordelen med disse er at de blir automatisk oppdatert av Python når brukeren endrer innholdet av for eksempel et tekstfelt.

I tillegg til *StringVar* har vi *IntVar* og *BooleanVar* og *DoubleVar*. Hvis datatypen til tekstfeltet du bruker ikke er en streng, men for eksempel en *int*, lønner det seg å bruke *IntVar* istedet. Da slipper du å konvertere mellom tall og strenger. Sjekk ut *alder*-feltet i eksempelapplikasjonen som bruker en *IntVar*.

Prosessen er som følger:
* Vi oppretter en StringVar (`tekst = StringVar()`)
* Vi *kan* sette en utgangsverdi på denne (`tekst.set("verdi")`)
* Så oppretter vi f.eks et tekstfelt med denne variabelen.
* Når en bruker f.eks trykker på en knapp kjøres det en funksjon. Da kan denne funksjonen bruke StringVar variabelen for å bruke innholdet av tekstfeltet.

Bruk eksempelapplikasjonen for å se hvordan dette gjøres i praksis.

# Widgets

En *widget* er en GUI-element vi bruker for å vise frem data og/eller for å la brukeren legge inn data. Eksempler på disse er:

![](images/Tkinter4.png)

* Knapper (Button). En knapp opprettes som regel med en linje kode som sier hvilket vindu det skal være i (root), hva som skal stå på knappen ("Søk"), og hvilken funksjon som skal kjøres når brukeren trykker på den (vis_tekst):

    ```python
    knapp = Button(root, text="Søk", command=vis_tekst)  
    ```

* Tekstfelter (Entry). Et teksfelt opprettes gjerne med to linjer kode; en for å lage en StringVar som holder på innholdet og en for å lage tekstfeltet (tekst). Også her angir vi hvilket vindu det skal være i (root), i tillegg til hvilken StringVar som skal brukes:

    ```python
    tekst = StringVar()  # Definerer en tekstvariabel for tekstfeltet 
    tekstfelt = Entry(root, textvariable=tekst)
    ```

* Ledetekst (Label) En Label inneholder vanligvis en ledetekst og brukes gjerne foran et tekstfelt (Entry). En *Label* kan imidlertid også inneholde et bilde (dette gjøres i eksempelapplikasjonen). Her er et eksempel med ledeteksten "Søk: ":

    ```python
    ledetekst = Label(root, text="Søk: ")
    ```

* Avsjekkingsboks (Checkbutton). Som med tekstfelter bruker også her en StringVar for å kunne vite hva brukeren har valgt. Denne vil inneholde enten *onvalue* eller *offvalue*, i dette tilfellet `"Y"` eller `"N"`,

    ```python
    sjekk = StringVar()
    sjekkboks = Checkbutton(root, text="Bruk filter", variable=sjekk, onvalue="Y", offvalue="N")
    ```

* Menyer. En meny (Menu) kan inneholde flere meny-elementer med en kommando (command) for hvert element som peker på hvilken funksjon som skal kjøres når elementet velges. Se i *person_main.py* i eksempelprogrammet for å se hvordan vi lager disse.
* Lister (Listbox). Lister er litt mer kompliserte da vi gjerne trenger en *scrollbar*. Vi oppretter en *Scrollbar* og en *Listbox* hver for seg og knytter dem sammen. I tillegg må vi legge til elementene i lista en og en. Bruk eksempelapplikasjonen som utgangspunkt for å bruke disse da det er litt komplisert.

    ```python
    scrollbar = Scrollbar(root, orient=VERTICAL)
    listbox = Listbox(root, yscrollcommand=scrollbar.set)
    scrollbar.config(command=listbox.yview)
    elements = ["egg","bacon","pølser"]
    for element in elements:
        listbox.insert(END, element)
    ```

* Nedrekksmeny (OptionMenu). En nedtrekksmeny trenger en StringVar for å holde på *valgt* verdi og en liste som angir hvilke valg som er mulige. Ofte vil det være aktuelt å hente denne listen fra en database.

    ```python
    chosen = StringVar()
    chosen.set("egg")
    options = ["egg","bacon","pølser"]
    nedtrekksmeny = OptionMenu(root, chosen, *options)  # Merk stjerna foran listevariabelen
    ```

Det finnes mange flere widgets man kan bruke. Det finnes også mange forskjellige biblioteker med forskjellige widgets, så utvalget er stort. For eksempel finnes det mange fine widgets for å la brukeren velge en dato. Bruke eventuelt nettet for å finne slike. Prøv f.eks et søk på `python date widget`.

# Grid

I eksempelkoden i forrige avsnitt mangler det noe. Vi har ikke *plassert* widget'ene i vinduet. Vi trenger derfor en kodelinje til per widget.

Vi kan plassere dem på flere måter, men vi skal bruke en metode, nemlig et rutenett. Hvis dere kan bruke `table`i html vil dere kjenne igjen prinsippet. Et element får en plassering med *column* og *row*, og så spesifiserer vi eventuelt hvor mange kolonner (*columnspan*) og hvor mange rader (*rowspan*) elementet skal spenne over. I tillegg kan vi legge til *luft* mellom elementene med *padx* og *pady*.

I følgende eksempel plasserer vi en ledetekst og et tekstfelt i hver sin rute, og en knapp i raden under som spenner over **to** kolonner

```python
from tkinter import *

# Definerer aksjon for knappetrykket
def vis_tekst():
    print(tekst.get())

# Opprett hovedvindu
root = Tk()

# Oppretter et tekstfelt brukeren kan skrive i
tekst = StringVar()  # Definerer en tekstvariabel for tekstfeltet 
tekstfelt = Entry(root, textvariable=tekst)

# Oppretter en ledetekst
ledetekst = Label(root, text="Søk: ")

# Oppretter en knapp med teksten "Søk", som kaller funksjonen vis_tekst() ved trykk
knapp = Button(root, text="Søk", command=vis_tekst)  

# Plasserer widget'ene i vinduet i et rutenett (grid) på 2x2 ruter
ledetekst.grid(column=0, row=0)
tekstfelt.grid(column=1, row=0)
knapp.grid(column=0, row=1, columnspan=2)  # Knappen skal gå over to kolonner

# Starter GUI'et
root.mainloop()
```

![](images/Tkinter1.png)

Rutenettet som ble brukt i dette eksempelet har to kolonner og to rader:

![](images/Tkinter2.png)

Merk at bredden på rutene tilpasser seg, etter størrelsen på knappene og tekstboksene vi putter inni dem. Merk også at søk-knappen har plassert seg midtstilt i de to rutene den har fått tildelt.

I tillegg kan vi si at det skal være 10 piksler foran og etter ledeteksten og 3 piksler over og under både ledeteksten og tekstfeltet:

```python
ledetekst.grid(column=0, row=0, padx=10, pady=3)
tekstfelt.grid(column=1, row=0, pady=3)
knapp.grid(column=0, row=1, columnspan=2)  # Knappen skal gå over to kolonner
```

![](images/Tkinter3.png)

Rutenettet for vinduet fra widgets-seksjonen settes sammen som følger:

```python
knapp.grid(column=0, row=0, padx=4, pady=4)
ledetekst.grid(column=0, row=1, padx=4, pady=4)
tekstfelt.grid(column=1, row=1, padx=4, pady=4)
sjekkboks.grid(column=0, row=2, columnspan=2, padx=4, pady=4)
nedtrekksmeny.grid(column=0, row=5, padx=4, pady=4)
listbox.grid(column=0, row=3, columnspan=2, rowspan=2, sticky=EW)
scrollbar.grid(column=3, row=3, rowspan=2, sticky=NS)
```

Og gir følgende grid:

![](images/Tkinter5.png)

Legg merke til at scrollbaren legges i en egen kolonne til høyre.

# Eksempelapplikasjon

Applikasjonen ligger på *GitHub*: [https://github.com/nilstes/simple-python-gui-app](https://github.com/nilstes/simple-python-gui-app). Dette er et vanlig sted å lagre kildekode på, og er et nyttig verktøy å bruke for å dele kildekode i et prosjekt. Dere kan laste ned koden ved å velge den grønne knappen til høyre med teksten `Clone or download`, og deretter `Download ZIP`. Pakk så ut zip-filen på maskinen din og åpne kodefilene fra Thonny. Det kan være lurt å legge disse kodefilene i en egen prosjekt-mappe på datamaskinen din for at det skal bli enklere å finne dem. Det vil det være lurt å gjøre for deres eget prosjekt også.

Dere kan kjøre applikasjonen ved å ha *person_main.py* fremme i Thonny og så trykke på start-knappen. Men før dere kjører må dere gjøre følgende:
* Opprette databasen fra [https://mysql.stud.iie.ntnu.no](https://mysql.stud.iie.ntnu.no). SQL-skriptet for dette ligger på GitHub-siden. Databasetabellen er en utvidelse av den vi brukte i forrige leksjon så dere må sannsynligvis slette den gamle tabellen først. Dette gjør dere med SQL-skriptet:

    ```sql
    DROP TABLE person;
    ```

* Sette inn eget brukernavn og passord helt øverst i filen *person_db.py*.
* Installere følgende biblioteker i Thonny hvis de ikke allerede er der: *matplotlib* og *numpy*.

Applikasjonen har 4 kodefiler. Kort oppsummert gjør de følgende:
* *person_main.py*: Dette er hovedfilen i applikasjonen og det er denne vi må ha fremme i Thonny for å starte applikasjonen. Filen inneholder hovedvinduet i applikasjonen, med widget'er og funksjonene som skal kjøres når vi trykker på *Søk* eller velger menyfunksjoner.
* *person_edit.py*: Denne filen innholder vinduet vi bruker for å endre **en** person eller opprette en ny. Den har widget'er for navn, adresse, alder osv., og det er mulig å laste opp et bilde for hver person.
* *person_db.py*: Denne filen inneholder all databasefunksjonalitet i applikasjonen. Her finner vi funksjoner for å søke på personer, for å oppdatere, opprette og slette personer, og for å finne statistikk om personer.
* *person_statistics.py*: Denne filen har to funksjoner for å lage stolpediagrammer. Her brukes bibliotekene *matplotlib* og *numpy*.

# Oppgaver

Oppgave denne uke blir å eksperimentere med eksempelapplikasjonen. 
* Prøv å endre plasseringen av widget'ene med *grid()*-funksjonen.
* Prøv å legge til nye knapper eller menyelementer og hvilke funksjoner som skal kjøres når brukeren velger disse.
