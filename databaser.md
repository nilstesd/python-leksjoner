# Innhold

*  [Bibliotek](#bibliotek)
*  [Eksempeldatabase](#eksempeldatabase)
*  [Databasekobling](#databasekobling)
*  [Select](#select)
*  [Søk](#søk)
*  [Insert](#insert)
*  [Update](#update)
*  [Delete](#delete)
*  [Bruke lister](#bruke-lister)
*  [Egen databasemodul](#egen-databasemodul)
*  [Oppgaver](#oppgaver)

# Eksempeldatabase

Denne leksjonen bruker en databasetabell, `person`, som eksempel. Du kan generere denne og litt testdata med dette SQL-skriptet:

```sql
CREATE TABLE IF NOT EXISTS `person` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`navn` varchar(256) NOT NULL,
	`adresse` varchar(256) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4;

INSERT INTO `person` (`id`, `navn`, `adresse`) VALUES
    (1, 'Nils Tesdal', 'Byen'),
	(2, 'Hei Sveisen', 'Byen'),
	(3, 'Bjørn Bjørnsen', 'Skogen');
```

Skriptet kan kjøres fra **phpMyAdmin** som du når på [https://mysql.stud.iie.ntnu.no](https://mysql.stud.iie.ntnu.no). Du skal allerede ha fått brukernavn og passord til denne databasen. Hvis ikke må du gi beskjed.

Du kjører skriptet ved å velge SQL-fanen på phpMyAdmin, lime inn skriptet og trykke *Utfør*-knappen nederst til høyre.

# Bibliotek

I tidligerer leksjoner har vi brukt bla *math*-biblioteket. Dette er innebygd i Python og vi trengte ikke gjøre noe annet enn å importere det i programfilen. Nå skal vi bruke et bibliotek som ikke er innebygd og vi må derfor legge det til som en pakke i IDE'en vår, Thonny.

Velg *Tools/Manage packages...* fra menyen og installer pakken **pymysql**.

Hvis installasjonen er vellykket skal `PyMySQL` komme opp i listen over installerte pakker:

![](images/Thonny17.png)

For å importere biblioteket i koden skriver vi dette øverst i kodefilen:

```python
import pymysql
```

Nå er vi klare til å bruke biblioteket.

# Databasekobling

Det første vi må gjøre når vi skal gjøre operasjoner mot en database er å opprette en databasekobling. Dette er en slags kommunikasjonskanal mellom programmet vårt og databasen. Vi bruker som regel brukernavn og passord for å opprette denne koblingen. Vi kobler opp til databasen på denne måten:

```python
db = pymysql.connect("mysql.stud.iie.ntnu.no","nilstesd","********","nilstesd")
```

Her kobler vi oss opp mot databasen på tjeneren `"mysql.stud.iie.ntnu.no"`, med brukernavn `"nilstesd"`, et passord og tilslutt databasenavnet `"nilstesd"`. Det er standard på skolens MySQL-database at databasenavnet er det samme som brukernavnet. Når du skal prøve dette må du erstatte `nilstesd` med ditt eget brukernavn og `********` med ditt eget passord.

Variabelen `db` har nå databasekoblingen vi skal bruke videre. Etter vi har kjørt spørringer mot databasen må vi **alltid** lukke databasekoblingen. Databasen har et begrenset antall koblinger å gi ut, og når vi lukker koblingen vil vi gjøre den tilgjengelig for noen andre.

Vi lukker koblingen med denne instruksjonen:

```python
db.close()
```

# Select

Nå kan vi begynne å kjøre SQL-setninger mot databasen.

For å kjøre spørringer trenger vi et *cursor*-objekt. En cursor bruker blant annet til å holde styr på hvilken linje vi har kommet til når vi leser resultatet av en spørring. Vi oppretter en cursor ved å hente den fra databasekoblingen:

```python
import pymysql

# Åpne databasekobling
db = pymysql.connect("mysql.stud.iie.ntnu.no","nilstesd","********","nilstesd")

# Lag en cursor for spørringen vi skal utføre
cursor = db.cursor()
```

Så bruker vi cursor-objektet for å kjøre spørringer med *execute()*:

```python
import pymysql

# Åpne databasekobling
db = pymysql.connect("mysql.stud.iie.ntnu.no","nilstesd","********","nilstesd")

# Lag en cursor for spørringen vi skal utføre
cursor = db.cursor()

# Kjør spørringen
cursor.execute("SELECT * from person")

# Gå gjennom resultatet linje for linje
for row in cursor:
    print(row)

# Lukk databasekobling
db.close()
```

```
(1, 'Nils Tesdal', 'Byen')
(2, 'Hei Sveisen', 'Byen')
(3, 'Bjørn Bjørnsen', 'Skogen')
```

Som vi ser i eksempelet bruker vi cursor-objektet til å gå gjennom resultatet av spørringen linje for linje.

I dette eksempelet skrev vi ut en hel rad om gangen, men vi kan også hente de enkelte feltene i en rad hver for seg:

```python
for row in cursor:
    id = row[0]  # Hent første kolonne på raden
    navn = row[1]  # Hent andre kolonne på raden
    adresse = row[2]  # Hent tredje kolonne på raden
    print("id=" + str(id) + ", navn: " + navn + ", adresse: " + adresse)
```

Utskriften blir nå:

```
id=1, navn: Nils Tesdal, adresse: Byen
id=2, navn: Hei Sveisen, adresse: Byen
id=3, navn: Bjørn Bjørnsen, adresse: Skogen

```
    
# Søk

I forrige avsnitt gjorde vi enkle oppslag hvor vi hentet all innholdet i en tabell. Som regel ønsker vi kun å hente et utvalg ved at vi utfører et søk. La oss si at vi ønsker å søke på alle personer med adresse `Byen`:

```python
import pymysql

# Åpne databasekobling
db = pymysql.connect("mysql.stud.iie.ntnu.no","nilstesd","********","nilstesd")

# Lag en cursor for spørringen vi skal utføre
cursor = db.cursor()

# Kjør spørringen
cursor.execute("SELECT * from person where adresse=%s", ("Byen"))

# Gå gjennom resultatet linje for linje
for row in cursor:
    print(row)

# Lukk databasekobling
db.close()
```

```
(1, 'Nils Tesdal', 'Byen')
(2, 'Hei Sveisen', 'Byen')
```

Det vi har gjort er å legge til `where adresse=%s` i selve SQL-setningen og så har vi lagt til `, ("Byen")` som andre argument til `cursor.execute()`. 

Dette er det samme som å skrive:

```python
cursor.execute("SELECT * from person where adresse='Byen'")
```

`%s` er en "placeholder" for en verdi. Det betyr at `%s` skal erstattes av en ordentlig verdi før spørringen kjøres. Vi putter de ordentlige verdiene i parantesen etter.

Den store fordelene med å bruke placeholdere er at vi da kan bruke variabler på denne måten:

```python
adresse = "Byen"
cursor.execute("SELECT * from person where adresse=%s", (adresse))
```

Vi kan legge på flere argumenter hvis vi ønsker det:

```python
adresse = "Byen"
navn = "Hei Sveisen"
cursor.execute("SELECT * from person where navn=%s AND adresse=%s", (navn, adresse))
```

# Insert

For å legge til nye rader i en tabell bruker vi `INSERT`:

```python
import pymysql

# Åpne databasekobling
db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd","********","nilstesd")

# Når vi setter på autocommit blir alle insert lagret i databasen med en gang
db.autocommit(True)

# Lag en cursor for spørringen vi skal utføre
cursor = db.cursor()

# Lagre SQL-setning i en streng
sql = "INSERT INTO person(navn,adresse) VALUES (%s, %s)"

# Kjør insert med verdier
cursor.execute(sql, ("Navnet", "Adressen"))

# Skriv ut antall rader som ble lagt til, kun for å sjekke at det gikk bra
print(cursor.rowcount)

# Lukk databasekobling
db.close()
```

Denne koden kan det være lurt å lage en funksjon av:

```python
import pymysql

def legg_til_person(navn, adresse):
    db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd","********","nilstesd")
    db.autocommit(True)
    cursor = db.cursor()
    sql = "INSERT INTO person(navn,adresse) VALUES (%s, %s))"
    cursor.execute(sql, (navn, adresse)  # Her bruker vi argumentene til funksjonen
    db.close()
    
legg_til_person("Hei Heisen", "På landet")
```

Når vi lager en funksjon ser vi den virkelige verdien av å bruke placegoldere i SQL-setningen. Nå kan vi bruke argumentene til funksjonen og sette disse inn i SQL-setningen ved hjelp av placeholdere.

# Update

For å oppdatere en eksisterende rad i databasen bruker vi `UPDATE`:

```python
import pymysql

def endre_adresse(navn, adresse):
    db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd","********","nilstesd")
    db.autocommit(True)
    cursor = db.cursor()
    sql = "UPDATE person set adresse=%s WHERE navn=%s"
    args = (adresse, navn)
    cursor.execute(sql, args) # Her bruker vi argumentene til funksjonen
    db.close()
    
endre_adresse("Bjør Bjørnsen", "På fjellet")
```

Her lager vi først en egen funksjon for å endre adresser, og så bruker vi denne funksjonen for å endre raden med *navn* lik *"Bjørn Bjørnsen"* slik at den nye adressen blir *"På fjellet"*.

# Delete

Og på samme måte bruker vi `DELETE` for å slette rader i databasen:

```python
import pymysql

def slett_person(navn):
    db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd","********","nilstesd")
    db.autocommit(True)
    cursor = db.cursor()
    sql = "DELETE FROM person WHERE navn=%s"
    cursor.execute(sql, (navn)) # Her bruker vi argumentet til funksjonen
    db.close()
    
slett_person("Hei Sveisen")
```

Her lager vi først en egen funksjon for å slette personer gitt et navn, og så kjører vi denne funksjonen for å slette personen med *navn* lik "Hei Sveisen".

# Bruke lister

Når vi kjører en spørring kan det hende at vi ikke ønsker å bruke resultatet med en gang, men bruke det et annet sted i koden. Da kan vi først lagre resultatet i en liste: 

```python
import pymysql

def hent_alle_navn():
    resultat = []
    db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd", "********", "nilstesd")
    cursor = db.cursor()
    cursor.execute("SELECT * from person")
    for row in cursor:
        resultat.append(row[1])
    db.close()
    return resultat

print(hent_alle_navn())
```
```
['Nils Tesdal', 'Hei Sveisen', 'Bjørn Bjørnsen']
```

Kanskje vi ønsker å putte alle disse navnene i et liste i et brukergrensesnitt? Da er det veldig lurt å bruke en variabel som holder på en liste. Da kan vi hente listen i en del av programmet, og bruke listen i en helt annen del:

*person_db.py*:
```python
import pymysql

# Funksjon for å hente alle navn fra databasen
def hent_alle_navn():
    resultat = []
    db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd", "********", "nilstesd")
    cursor = db.cursor()
    cursor.execute("SELECT * from person")
    for row in cursor:
        resultat.append(row[1])
    db.close()
    return resultat
```

*gui_app.py*:
```python 
from tkinter import *
import person_db

# Opprett hovedvindu i GUI
top = Tk()

# Hent navn fra databasen
liste = person_db.hent_alle_navn()

# Opprett listebox i GUI
listeboks = Listbox(top)

# Fyll listeboksen med navnene fra databasen
for element in liste:
    listeboks.insert(END, element)
    
# Plasser listeboks i vinduet (her sier vi ingenting om hvor)
listeboks.pack()

# Start GUI-applikasjonen
top.mainloop()
```

Her er det endel ukjent kode for å opprette GUI. Ikke bry dere om dette nå, vi kommer tilbake til GUI-kode i neste leksjon. Men observer at vi bruker `person_db.hent_alle_navn()` for å hente navnene fra databasen i den ene modulen, for deretter å putte de inn i listeboksen i den andre modulen.

Men prøv gjerne eksempelet over selv!

## Tabell

I eksempelet under lager vi til og med en todimensjonal liste, det vil si en liste med lister, som i praksis er en **tabell** med data:

```python
import pymysql

def finn_personer(adresse):
    resultat = []
    db = pymysql.connect("mysql.stud.iie.ntnu.no", "nilstesd","********","nilstesd")
    cursor = db.cursor()
    cursor.execute("SELECT * from person WHERE adresse=%s", (adresse))
    for row in cursor:
        linje = []
        linje.append(row[0])
        linje.append(row[1])
        linje.append(row[2])
        resultat.append(linje)
    db.close()
    return resultat

print(finn_personer("Byen"))
```

Utskriften blir som følger (OBS! dere kan ha andre data i databasen ut ifra hvilke *insert*, *update* eller *delete* dere har kjørt):

```
[[1, 'Nils Tesdal', 'Byen'], [2, 'Hei Sveisen', 'Byen']]
```

Vi ser at den *ytre* listen inneholder to elementer, som begge er lister i seg selv. Den ytre listen inneholder to personer, mens de indre listene inneholder detaljer om hver person.

Vi kan også ta kontrollen over utskriften selv:

```python
liste = finn_personer("Byen")
for rad in liste:
    print(rad)
```

```
[1, 'Nils Tesdal', 'Byen']
[2, 'Hei Sveisen', 'Byen']
```

Eller slik:

```python
for rad in liste:
    for felt in rad:
        print(felt)
```

```
1
Nils Tesdal
Byen
2
Hei Sveisen
Byen
```

Eller vi kan lage samme utskrift som vi gjorde i et tidligere eksempel:

```python
for rad in liste:
    id = rad[0]
    navn = rad[1]
    adresse = rad[2]
    print("id=" + str(id) + ", navn: " + navn + ", adresse: " + adresse)
```

```
id=1, navn: Nils Tesdal, adresse: Byen
id=2, navn: Hei Sveisen, adresse: Byen
```

# Egen databasemodul

Det er lurt å legge kode som omhandler ulike ting i ulike filer. Da blir koden lettere å håndtere. Vi kan for eksempel legg all kode som har med databasen å gjøre i en egen fil, *database.py*. Men det kan hende at også dette blir mye kode og vi kan dele opp ytterligere. La oss derfor lage en fil, *person_db.py* som inneholder all databasekode som aksesserer `person`-tabellen.

Denne filen kan for eksempel ha følgende funksjoner:

```python
def opprett_person(navn, adresse):
    # kode her

def finn_personer(adresse):
    # kode her

def hent_alle_navn():
    # kode her

def endre_adresse(navn, adresse):
    # kode her
    
def slett_person(navn):
    # kode her
```

Denne kunne inneholdt flere eller færre funksjoner, alt utifra hva vi har behov for i applikasjonen vår.

# Oppgaver

## Oppgave 1

Implementer (eller kopier fra leksjonen) alle 5 funksjonene som er foreslått i det siste avsnittet, og lagre resultatet i filen *person_db.py*.

## Oppgave 2

Du skal nå lage en *test* av koden du skrev i oppgave 1.

Lag en ny fil, *test_db.py*, som skal bruke alle funksjonene i oppgave 1. 

Opprett først noen få personer med `opprett_person()`, så bruker du funksjonene for å hente og oppdatere, og tilslutt sletter du alle radene du har opprettet i testen fra databasen ved å bruke funksjonen `slett_person()`.

