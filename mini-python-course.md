# Innhold

* [Introduksjon](#introduksjon)
 * [IDE](#ide)
 * [Hello World](#hello-world)
 * [Instruksjoner](#instruksjoner)
 * [Kodefeil](#kodefeil)
 * [Løkker](#løkker)
 * [Variabler](#variabler)
 * [Input](#input)
 * [Oppgaver](#oppgaver1)
* [Kommentarer](#kommentarer)
* [Datatyper](#datatyper)
 * [Streng](#streng)
 * [Tall](#tall)
 * [Boolean](#boolean)
 * [Liste](#liste)
 * [Oppgaver](#oppgaver2)
* [Kontrollflyt](#)
 * [If-Else](#if-else)
 * [Elif](#elif)
 * [Innrykk](#innrykk)
 * [For-løkker](#for-løkker)
 * [While-løkker](#while-løkker)
 * [Funksjoner](#funksjoner)
 * [Auto completion](#auto-completion)
 * [Oppgave](#oppgaver3)
* [Bruke biblioteker](#bruke-biblioteker)

# Introduksjon

I denne leksjonen skal vi gå gjennom grunnleggende programmering med Python.

## IDE
Man kan skrive kode med en enkel teksteditor, men det er lettere å bruke et program som er spesiallaget til dette formålet. Derfor bruker vi gjerne en *IDE* (Integrated Development Environment). Det finnes flere IDE'er som er brukbare for hvert språk, og noen IDE'er kan brukes til flere programmeringsspråk. Vi skal bruke en enkel IDE som er spesiallaget for Python-undervisning. Denne IDE'en heter "Thonny" og vi kan laste den ned herfra: [http://thonny.org](http://thonny.org). Hvis noen ønsker å bruke en mer avansert IDE er det mulig å bruke andre, f.eks [PyDev](http://www.pydev.org/),
 men jeg vil anbefale å bruke Thonny da det er denne som brukes i undervisningen.

Last ned Thonny og følg instruksjonene på nettsiden for å installere den på din maskin. Når du så starter Thonny vil du få opp et vindu som ser slik ut (Windows):

![](images/Thonny1.png)


Dette vinduet har 3 ruter. I ruten øverst til venstre skriver vi kode, i ruten under ser vi resultatet når vi kjører koden, og ruten til høyre brukes for "debugging". Vi skal se litt på debugging tilslutt i denne leksjonen. 

Hvis du ikke ser "Variables"-vinduet kan du velge `View/Variables` fra menyen.

## Hello World

Nå kan vi lage vårt første program i Python. Skriv følgende kode i ruten øverst til venstre:

```python
print("Hei verden!")
```

Dette er et enkelt men fullverdig Python-program hvor vi ganske enkelt sier at programmet skal skrive ut teksten "Hei verden!". For at vi skal kunne kjøre programmet må vi først lagre det i en fil. Velg *File/Save* eller trykk på diskett-knappen, velg en passende mappe og bruk f.eks filnavnet *hello.py*. Vi bruker alltid filetternavnet *.py* for Python-programmer.

Kjør programmet ved å trykke på den grønne start-knappen på verktøylinja. Da vil du se at teksten "Hei verden!" blir skrevet ut i ruten nederst til venstre:

![](images/Thonny3.png)

Merk at koden i Thonny blir fargelagt etter visse regler. Dette kalles "syntax highlighting" og gjør at det blir lettere å lese koden.

## Instruksjoner

Et program består av en serie med instruksjoner (eller kommandoer). Vi forteller Python hvordan en oppgave skal utføres ved å gi den en rekke instruksjoner, litt på samme måte som en matoppskrift gir oss instruksjoner om hvordan vi skal lage en rett.

I den virkelige verden kunne man ha laget instruksjoner for å betale i en selvbetjent kasse i butikken:

```
Så lenge det er varer i handlevogna:
    ta ut en vare
    scan strekkode
trykk på "Ferdig"
utfør betaling
```

Dette er ikke et Python-program men bruker samme oppbygning og kalles gjerne *pseudo-kode*. For Python og andre programmeringsspråk er det mer eller mindre strenge regler for hvordan man skal skrive koden.

## Kodefeil

Hvis vi lager feil i koden får vi feilmeldinger fra Python. Vi kan for eksempel skrive `printt` istedetfor `print`. `printt` er ikke en instruksjon Python kjenner til og derfor vil vi få en feilmelding:

![](images/Thonny3-2.png)

Når vi skriver ut tekst som vi gjør her bruker vi en tekststreng, `"Hei Verden!"`. En tekststreng må alltid ha et anførselstegn på hver side. Prøv å ta bort det ene og se hva som skjer da.

## Løkker

La oss prøve å gjøre noen endringer på programmet vi har skrevet. Hvordan kan vi f.eks skrive ut teksten "Hei verden!" flere ganger? Den aller enkleste løsningen er å endre programmet slik at vi skriver samme `print`-kommando flere ganger:

![](images/Thonny4.png)

Men det finnes mer elegante måter å gjøre dette på. Vi kan heller lage en **løkke**:

```python
for i in range(0, 3):
    print("Hei verden!")
```

Dette gir akkurat samme resultat som før. 

I dette eksempelet introduserte vi to nye konsepter, *variabler* og *løkker*. Vi går ikke i dybden på dette nå, men kommer tilbake til det i senere leksjoner. Likevel, med pseudokode kan vi si det på denne måten:

```{python, eval = FALSE}
utfør 3 ganger:
    print("Hei verden!")
```

Men vi introduserte også variabelen `i` som inneholder et tall. En litt mer nøyaktig beskrivelse av hva som skjer kan være slik:

```{python, eval = FALSE}
utfør 3 ganger, først med i=0, så med i=1, så med i=2:
    print("Hei verden!")
```

For å se at verdien på `i` endrer seg kan vi også skrive ut verdien av variabelen i koden. Legg til kommandoen `print(i)` på denne måten:

```python
for i in range(0, 3):
    print("Hei verden!")
    print(i)
```

Siden begge `print`-kommandoene er har samme innrykk, dvs at det er samme antall mellomrom foran kommandoen, vil begge `print`-kommandoene bli utført som en del av løkka.

Når vi kjører programmet ser vi dette:

![](images/Thonny5.png)

Første gang teksten skrives ut er `i` 0, så 1, så 2. Merk at den blir aldri 3. Når vi lager løkken sier vi at `i` først skal være 0, så skal den økes med **en** for hver gang, så lenge den **er mindre** enn 3.

Vi kan endre innrykket slik at den andre `print`-kommandoen ikke er en del av løkka. Her utføres den andre kommandoen etter at løkka er ferdig:

![](images/Thonny5-2.png)

## Variabler

Vi brukte variabelen `i` for å lage en løkke, men vi kan bruke variabler på flere måter, for eksempel til å utføre enkel regning:

![](images/Thonny6.png)

Her setter vi først variabelen `a` til 5, så setter vi variabelen `b` til 6. Deretter setter vi variabelen `c` lik `a + b` som da i praksis blir `11`. På neste linje setter vi `c` lik `c + 2`. Vi skal altså øke verdien av `c` med 2. Siden `c` er 11 før denne kommandoen, blir `c` 13 etter kommandoen er utført. På siste linje skriver vi ut verdien av `c` som nå er 13.

Merk innholdet i ruten til høyre. Denne viser nå verdien av alle variablene slik de var på slutten av eksekveringen (kjøring) av koden. Vi kan også bruke denne ruten til å vise verdien av variablene mens vi kjører. Dette kaller vi "debugging".

Vi bruker variabler for å lagre verdier. I følgende eksempel sier vi at variabelen `a` skal ha verdien 5.

```python
a = 5
```

I navnet *variabel* ligger det at verdien kan endres. Det kan vi gjøre på følgende måte:

```python
a = a + 5
```

Hvis `a` var 5 i utgangspunktet, vil `a` bli 10 etter å har kjørt denne instruksjonen.

Ofte bruker man enkle navn på variabler, som `a` eller `b`, men det er egentlig en bedre praksis å bruke skikkelige navn. Si at vi skal regne ut arealet på et rektangel. Vi kan da introdusere to variabler, `lengde` og `bredde`:

```python
lengde = 10
bredde = 2
areal = lengde * bredde
```

Koden over er lettere å forstå enn om vi hadde brukt navnene `a` og `b`. Da blir det også lettere å skrive kode uten feil, og det blir lettere for andre å lese koden. Når man programmerer gjør man gjerne det i team, og da er det kjempeviktig at kode er lettlest sånn at alle på teamet kan forstå koden. Da kan de andre også endre denne koden uten stor fare for å innføre feil.

## Input

For å lage et interaktivt program kan vi spørre brukeren om input:

![](images/Thonny13.png)

Kommandoen på den første linjen sier at vi skal skrive ut teksten `"Skriv inn et tall: "` og deretter vente på at brukeren skriver inn et tall. Når brukeren har skrevet tallet og trykket på "Enter" vil variabelen `a` ha verdien som brukeren skrev inn.

![](images/Thonny14.png)

<a name="oppgaver1"/>
## Oppgaver

Nå skal dere prøve å løse noen konkrete oppgaver. Bruk kodeeksemplene i leksjonen som utgangspunkt.

### Oppgave 1

Lag et program som starter slik:

```python
a = 3
b = 2
```

Deretter skal det gjøre følgende: 
* La `c`  bli `a` ganger `2`
* La `d` bli `b` pluss `c`
* Gjør verdien av `d` til det dobbelte av hva den er
* Skriv ut verdien av `d`

Er verdien av `d` `16`?

Hvis ikke må du bruke debuggeren til å finne ut hvorfor.

### Oppgave 2

Nå skal du utvide programmet fra oppgave 1.

Legg til denne linjen på slutten:


```python
e = input("Skriv inn enn tekst: ")
```

Deretter skal programmet skrive ut verdien av `e` like mange ganger som verdien av `d`.

Kjør programmet med debuggeren og observer hvordan løkketelleren øker underveis.

# Kommentarer

Før vi begynner med datatyper skal vi se på *kommentarer*. Kommentarer gjør ingenting i selve koden, men vi kan bruke dem for å forklare hva som skjer i koden. Da kan koden bli mer lesbar og det da blir lettere for andre å forstå koden. Dette blir spesielt viktig når programmet blir stort og uoversiktlig. Vi starter kommentarer med tegnet `#`:

```python
# Lag variabler for bredde og lengde
bredde = 10
lengde = 20

# Utfør regneoperasjon med begge tallene
areal = bredde * lengde

# Skriv ut resultatet
print("Arealet er : " + str(areal))
```

I tillegg til å skrive kommentarer på hele linjer som i eksempelet over, kan vi legge inn kommentarer på slutten av vanlige kodelinjer:

```python
bredde = 15  # Bredden på rektangelet
lengde = 10  # Lengden på rektangelet
```


# Datatyper

Alle variablene vi har brukt til nå har representert tall, men de kan også representere andre *typer* data. Datatyper er sentralt i alle programmeringsspråk, men man håndterer de på forskjellig måte. I Python trenger man ikke si hvilken type en variabel skal ha, man gir bare variablene verdier direkte. Python holder likevel orden på hvilken datatype en variabel har.

## Streng

La oss først se på *strenger* som vi bruker for å representere tekst. Strenger har alltid anførseltegn på hver side teksten. Her sier vi at variabelen `melding` skal inneholde teksten `"Hei verden"`, så skal vi skrive ut verdien av variabelen: 

```python
melding = "Hei verden!"
print(melding)
```

Dette gir utskriften:

```
Hei verden!
```

På samme måte som denne litt enklere koden:

```python
print("Hei verden!")
```

### Konkatenering

Konkatenering betyr å legge sammen strenger. Dette gjør vi enkelt i Python ved å bruke `+`-operatoren:

```python
melding = "Hei"
melding = melding + " "
melding = melding + "Ola"
melding = melding + "!"
print(melding)
```

Gir utskriften:

```
Hei Ola!
```

Vi kan også legge sammen strenger og tall, men da må vi gjøre noe ekstra. Følgende kode gir oss feilmelding fordi det i utgangspunktet ikke er lov å legge sammen en streng og et tall:

```python
egg = 5
melding = "Du har " + egg + " i kurven"
print(melding)
```

Hvis vi skal gjøre dette riktig må vi konvertere tallet til en streng først. Det kan vi gjøre med funksjonen `str()` på denne måten:

```python
egg = 5
egg = str(egg)
melding = "Du har " + egg + " i kurven"
print(melding)
```

På den andre linja endrer vi datatypen slik at `egg` inneholder strengen `"5"` istedetfor tallet `5`. Når vi konkatenerer på neste linje er det bare strenger som legges sammen. Vi kan også gjøre dette på en litt raskere måte og konvertere akkurat der vi trenger det:

```python
egg = 5
melding = "Du har " + str(egg) + " i kurven"
print(melding)
```

Begge gir utskriften:

```
Du har 5 egg i kurven
```

### Strengfunksjoner

I forrige eksempel brukte vi funksjonen `str()` som er innebygd i Python. Vi skal se mer på funksjoner i en senere leksjon, men følgende er 3 innebygde funksjoner for behandling av strenger:

```python
egg = 5
melding = "Du har " + str(egg) + " egg i kurven"
print(melding.upper())
print(melding.lower())
print(len(melding))
```

Dette gir følgende utskrift:

```
DU HAR 5 EGG I KURVEN
du har 5 egg i kurven
21
```

* `upper()` konverterer alle små bokstaver i strengen til store bokstaver.
* `lower()` konverterer alle store bokstaver i strengen til små bokstaver.
* `len()` gir lengden av strengen (antall tegn) som et tall.

I koden er det en ting som er verdt å merke seg og som kan være forvirrende. For å bruke funksjonen `len()` putter vi strengen vi skal finne lengden på inn i parantesene. For å bruke `upper()` og `lower()` slenger vi disse bakpå variabelen med et punktum imellom. 
Dette er fordi `len()` (og også `str()`) er en innebygd funksjon, mens `upper()` og `lower()` egentlig er *metoder* på streng-objektet. Dere trenger *ikke* å bry dere om denne distinksjonen da dette er utefor skopet til dette kurset og dere kan heretter bare referere til begge som *funksjoner*.

Vi kan også trekke ut delstrenger fra en streng. Se følgende eksempel:

```python
egg = 5
melding = "Du har " + str(egg) + " egg i kurven"
print(melding[0:2])
print(melding[3:6])
```

Dette gir følgende utskrift:

```
Du
har
```

`melding[0,2]` plukker ut tegnene fra plassering 0 (aller først i strengen) til men ikke inkludert plassering 2. Da får vi med oss tegnene `D` på plassering 0 og `u` på plassering 1. `melding[3:6]` tar med tegnene fra plassering 3, 4 og 5 som gir `"har"`.

Python har langt flere innebygde strengfunksjoner. Prøv f.eks. å Google "python strings" og dere vil finne mye materiale på dette.

## Tall

Forskjellige programmeringsspråk kan ha forskjellige datatyper for tall, men alle skiller mellom heltall og desimaltall. I Python har vi to typer:

* int (Integer) for *heltall*.
* float (Floating point) for *desimaltall*.

Legg merke til at vi bruker `.` som desimalskilletegn og ikke `,`. Dette er vanlig i de fleste programmeringsspråk.

La oss se på følgende kode:

```python
a = 5
b = 6.5
c = a + b
print(a)
print(type(a))
print(b)
print(type(b))
print(c)
print(type(c))
```

Dette gir følgende utskrift:

```
5
<class 'int'>
6.5
<class 'float'>
11.5
<class 'float'>
```

Vi bruker her funksjonen `type()` som er innebygd i Python og gir oss datatypen til en variabel.

Her er det flere ting å merke seg i koden:
* Datatypen til `a` bestemmes av at vi gir den verdien `5`, som er et heltall, og datatypen blir derfor en *int*.
* Datatypen til `b` bestemmes av at vi gir den verdien `6.5`, som er et desimaltall, og datatypen blir derfor en *float*.
* Det er lov til å legge sammen en *int* og en *float*. Den resulterende datatypen blir da *float*.
* Addisjon, subtraksjon og multiplikasjon med to *int* resulterer alltid i en *int*.
* Divisjon med to *int* resulterer i en *float*.
* Hvis det er en *float* involvert i aritmetikken blir resultatet alltid en *float*.

### Aritmetikk

Vi kan utføre flere aritmetiske funksjoner enn bare addisjon:

```python
a = 10
b = 2
print(a-b)
print(a*b)
print(a/b)
```

Dette gir utskriften:

```
8
20
5.0
```

Python har også innebygde matematiske funksjoner:

```python
a = 6.6
print(round(a))
print(pow(a,2))
```

```
7
43.559999999999995
```

Den første funksjonen gjør en avrunding til nærmeste heltall, mens den andre funksjonen utfører *a opphøyd i 2*.

I tillegg finnes det et bibliotek som heter *math* og inneholder veldig mange matematiske funksjoner. Et bibliotek er en samling med ekstra funksjonalitet som vi kan velge å benytte oss av. For å bruke et bibliotek må vi importere dette i kode. Vi skriver `import math` øverst i kodefila vår:

```python
import math

a = 6.6
print(math.floor(a))
print(math.ceil(a))
```

Dette gir utskriften:

```
6
7
```

* På den første linjen importerer vi biblioteket *math*.
* Når vi skal bruke biblioteket skriver vi `math.` og så funksjonen vi ønsker å bruke.
* I math-biblioteket har vi 2 funksjoner for avrunding, en som alltid runder av nedover, *floor()* og en som alltid runder av oppover, *ceil()*.

For å finne alle funksjonene i *math*-biblioteket kan dere se her: [https://docs.python.org/3/library/math.html](https://docs.python.org/3/library/math.html)

## Boolean

Den neste datatypen vi skal se på er *bool* eller *boolean* som er enten `True` eller `False`. Denne datatypen brukes i logiske uttrykk hvor vi ønsker å finne ut om noe er *sant* eller *usant*. Vi kommer tilbake til dette når vi skal se på beslutninger, men vi skal nå se på noen enkle eksempler:

```python
a = 5 > 6
print(a)
print(type(a))
```

Gir utskriften: 

```
False
<class 'bool'>
```

Dette er fordi uttrykket `5 > 6` er **usant**, fordi 5 er ikke større enn 6. På den andre linjen skrives datatypen ut.

Vi kan bruke følgende sammenligningsoperatorer:

* < mindre enn
* > større enn
* <= mindre enn eller lik
* >= større enn eller lik
* == er lik
* != er ikke lik

Eksempler:
* `5 < 6` er `True` fordi 5 **er** mindre enn 6
* `5 > 6` er `False` fordi 5 **er ikke** større enn 6
* `5 == 5` er `True` fordi 5 **er** lik 5
* `5 == 6` er `False` fordi 5 **er ikke** lik 6
* `5 <= 6` er `True` fordi 5 **er** mindre enn eller lik 6 (mindre)
* `5 <= 5` er `True` fordi  5 **er** mindre enn eller lik 5 (lik)
* `6 >= 6` er `True` fordi  6 **er** større enn eller lik 6 (lik)
* `6 >= 5` er `True` fordi  6 **er** større enn eller lik 5 (større)

Vi kan sammeligne strenger også:
* `"hei" == "hei"` er `True` fordi strengene **er** like
* `"hei hei" == "hei"` er `False` fordi strengene **er ikke** like
* `"hei hei" != "hei"` er `True` fordi strengene **er ikke** like
* `"Arne" < "Bård"` er `True` fordi Arne **er før** Bård alfabetisk
* `"Chris" < "Bård"` er `False` fordi Chris **er ikke før** Bård alfabetisk


## Liste

Så langt har variablene vi har brukt vært tall eller strenger eller boolske verdier. Men vi kan også ha variabler som har **flere** verdier, altså en **liste**, også kalt **tabell**.

I eksempelet under oppretter vi en liste med 4 strenger og utfører flere operasjoner på den:

```python
# Opprett en liste med 4 elementer
liste = ["Fredrik", "Arne", "Karl", "Åge"]

liste.append("Tore")  # Legg til på slutten av lista
print(liste)

liste.remove("Tore")  # Ta bort elementet "Tore"
print(liste)

liste.pop(2)  # Ta bort elementet på posisjon 2 (første posisjon er 0)
print(liste)

liste.insert(1, "Bendik")   # Legg til "Bendik" på posisjon 1
print(liste)

liste.sort()  # Sorter listen
print(liste)

liste.reverse()  # Snu listen opp ned
print(liste)

antall = len(liste)  # Finn antall elementer i listen
print(antall)
```
```
['Fredrik', 'Arne', 'Karl', 'Åge', 'Tore']
['Fredrik', 'Arne', 'Karl', 'Åge']
['Fredrik', 'Arne', 'Åge']
['Fredrik', 'Bendik', 'Arne', 'Åge']
['Arne', 'Bendik', 'Fredrik', 'Åge']
['Åge', 'Fredrik', 'Bendik', 'Arne']
4
```

Operasjonene vi utfører er:
* **liste.append(element)** - legger til et element på slutten ac lista
* **liste.remove(element)** - sletter elementet gitt i argumentet
* **liste.pop(posisjon)** - sletter elementet på gitt posisjon. Merk at første posisjon i lista er **0**
* **liste.insert(posisjon, element)** - setter inn et nytt element på gitt posisjon. Elementene på høyere posisjoner blir forskjøvet.
* **liste.sort()** - sorter lista
* **liste.reverse()** - reverser rekkefølgen i lista
* **len(liste)** - gir antall elementer i lista

Vi kan lage lister med tall istedetfor strenger:

```python
liste = [1, 67 , 18, 5]
```

Vi kan aksessere enkeltelementer i en liste på denne måten:

```python
liste = [1, 3, 5]

sum = liste[0] + liste[1] + liste[2]
print(sum)
```
```
9
```

Første element har alltid indeks 0.


Vi kan også opprette en helt tom liste og bygge den opp med brukerinput:

```python
liste = []
tall = input("Skriv inn et tall: ")
liste.append(int(tall))
tall = input("Skriv inn et tall: ")
liste.append(int(tall))
print(liste)
```

Merk at vi her konverterer input-strengen til et tall før vi legger den i lista. Dette kan være greit hvis vi skal gjøre aritmetiske operasjoner på elementer i lista senere.

Selv om det vanligste er å lage lister hvor alle elementene har samme datatype, er det også mulig å ha lister hvor hvert element har forskjellige datatyper:

```python
liste = ["Karl", "Karlsen" , 18, True]
```

Merk at hvis lista har forskjellige datatyper, så kan vi ikke sortere lista siden man ikke kan sammenligne strenger med tall.


<a name="oppgaver2"/>
## Oppgaver

### Oppgave 1

Lag et program som lar brukeren skrive inn tre strenger som legges i hver sin variabel. Deretter skal programmet slå sammen strengene til en og så skal strengen skrives ut, med store bokstaver og utropstegn bak!

### Oppgave 2

Lag et program som lar brukeren skrive inn fire tall. Hvert av disse tallene skal legges inn i en liste. Når brukeren har skrevet inn tallene så skal følgende skrives ut:

* Tallene som ble skrevet inn, i sortert rekkefølge.
* Summen av tallene
* Gjennomsnittet av tallene

Hvis tallene brukeren skriver inn er [12, 4, 8, 3], så skal utskriften bli:

```
Tall: 3,4,8,12
Sum: 27
Gjennomsnitt: 6.75
```

# Kontrollflyt

I dette kapittelet skal vi lære om kontrollflyt, rekkefølgen på instruksjonene i et program og hvordan vi kan endre denne utifra hva som skjer i programmet. I forrige leksjon snakket vi om boolske variabler og logiske uttrykk. Det er nettopp slike vi skal bruke for å ta avgjørelser underveis i et program.

## If-Else

Den mest klassiske måten å ta en avgjørelse i et dataprogram på, er å bruke instruksjonen `if`. De aller fleste programmeringsspråk har denne instruksjonen. Med pseudokode kan vi si det slik:

```
hvis noe er sant så:
    gjør noe
ellers:
    gjør noe annet
```

Vi kan prøve oss på et Python-program:

```python
tall = input("Skriv inn et tall: ")
tall = int(tall)
if tall < 5:
    print("Tallet er mindre enn 5")
else:
    print("Tallet er ikke mindre enn 5")
```

Her lar vi først brukeren skrive inn et tall, så konverterer vi input'en fra tekst til et tall. Deretter sier vi at hvis tallet i variabelen `tall` er mindre enn 5 så skal vi utføre instruksjonen på linja rett under og skrive ut teksten `"Tallet er mindre enn 5"`. Hvis ikke, altså hvis `tall` er 5 eller større, så skal vi skrive ut teksten `"Tallet er ikke mindre enn 5"`.

## Elif

`elif` er en kortversjon av *else if*. Vi kan bruke dette for å lage flere alternativer i samme *if*-setning:

```python
tall = input("Skriv inn et tall: ")
tall = int(tall)
if tall < 5:
    print("Tallet er mindre enn 5")
elif tall == 5:
    print("Tallet er 5")
elif tall == 6:
    print("Tallet er 6")
else:
    print("Tallet er større enn 6")
```

Dette kan uttrykkes med pseudokode på følgende måte:

```
hvis variabelen tall er mindre enn 5 så:
    skriv ut "Tallet er mindre enn 5"
ellers, hvis variabelen tall er lik 5 så:
    skriv ut "Tallet er 5"
ellers, hvis variabelen tall er lik 6 så:
    skriv ut "Tallet er 6"
ellers, hvis ingen av uttrykkene over er sann, så:
    skriv ut "Tallet er større enn 6"
```

Vi kan bruke så samme mange `elif` vi vil inne i en *if*-setning.


## Innrykk

Når vi skal utføre en eller flere instruksjoner som et resultat av en avgjørelse, for eksempel ved bruk av en `if`-setning, så bruker vi *innrykk* foran denne/disse instruksjonen(e). Et innrykk kan være ett eller flere mellomrom.

Det er viktig å bruke **samme** innrykk i et program. Dette gjør at koden blir mer leselig, og det blir lettere å unngå feil. Bruk for eksempel alltid 4 mellomrom for innrykk, eller ett trykk på `tab`-knappen.

I eksempelet i forrige avsnitt utførte vi kun *en* instruksjon etter `if` eller `elif` eller `else`. Men her kan vi like gjerne utføre flere instruksjoner. For å få til dette må disse instruksjonene ha samme innrykk. La oss se på følgende eksempel:

```python
tall = input("Skriv inn et tall: ")
tall = int(tall)
if tall<5:
    print("Tallet er mindre enn 5")
    print("Kanskje du skal prøve et større tall")
else:
    print("Tallet er ikke mindre enn 5")
    print("Det betyr at enten er tallet 5")
    print("eller så er det større")
print("Nå er vi ferdig")
```

* Her sier vi at hvis variabelen `tall` er mindre enn 5, så skal instruksjonene på de **to** neste linjene utføres, fordi disse to instruksjonene bruker samme innrykk.
* Hvis ikke, altså hvis tallet er 5 eller større så skal instruksjonene på de tre linjene etter `else:` utføres.
* MEN, instruksjonen på den aller siste linjen skal utføres uansett, både hvis tallet er mindre enn 5 eller ikke.

Eksperimenter ved å kjøre dette programmet noen ganger, med forskjellig innrykk på de forskjellige linjene.

## For-løkker

Vi lagde løkker allerede i introduksjonsleksjonen. La oss se på samme eksempel en gang til:

```python
for i in range(0, 3):
    print("Hei verden!")
```
```
Hei verden!
Hei verden!
Hei verden!
```

* Vi lager en variabel, `i`, som skal være 0 når vi starter løkka.
* Så utfører vi instruksjonene på linjen(e) etter kolonet. 
* Deretter øker vi verdien av `i` med 1 og sjekker om `i` fremdeles er mindre enn 3.
* Hvis den er det så utfører vi instruksjonen(e) en gang til, øker `i` med 1 og sjekker en gang til.
* Hvis ikke avsluttes løkka

Vi kan også bruke variabler. Følgende er i praksis helt likt det forrige:

```python
start = 0
slutt = 3
for i in range(start, slutt):
    print("Hei verden!")
```

Vi trenger ikke starte med 0:

```python
for i in range (2, 5):
    print(i)
```
```
2
3
4
```

Vi kan også telle nedover ved å legge på et tredje argument, `-1`:

```python
for i in range (5, 2, -1):  # Starter på 5, holder på til i er 2,
    print(i)
```
```
5
4
2
```

Og vi trenger ikke øke eller minske med 1:

```python
for i in range (2, 6, 2):  # Starter på 2, holder på til i er 6, øker med 2
    print(i)
```
```
2
4
```

På samme måte som i avsnittet om `if`, kan vi utføre *flere* enn en instruksjon i løkka ved å bruke innrykk:

```python
for i in range (0, 2):
    print("Inne i løkka 1")
    print("Inne i løkka 2")
print("Ferdig med løkka")
```
```
Inne i løkka 1
Inne i løkka 2
Inne i løkka 1
Inne i løkka 2
Ferdig med løkka
```

### For-løkke med lister

Det finnes en veldig enkel metode for å traverse lister:

```python
liste = ["Fredrik", "Arne", "Karl", "Åge"]
for navn in liste:
    print(navn)
```
```
Fredrik
Arne
Karl
Åge
```

Med pseudokode bli dette:
```
for hvert navn i lista:
    skriv ut navnet
```

Det som skjer her er at vi lager en løkke som som itererer like mange ganger som det er elementer i lista. I tillegg lager vi en variabel `navn` som får verdien av det elementet i lista man er på. 

## While-løkker

For-løkker brukes når vi skal gjøre noe et bestemt antall ganger. Men noen ganger vet vi ikke på forhånd hvor mange ganger vi skal utføre noe. Da kan vi si at vi skal utføre noe *så lenge noe er sant*. Til det bruker vi instruksjonen *while*:

```python
tall = input("Skriv inn et tall mellom 0 og 9: ")
while int(tall) < 0 or int(tall) > 9:
    print("Tallet er ikke mellom 0 og 9. Prøv på nytt!")
    tall = input("Skriv inn et tall: ")
print("Tusen takk!")
```

Vi kan uttrykke dette med pseudokode:

```
så lenge variabelen `tall` er mindre enn 0 eller større enn 9, utfør instruksjonene under:
    skriv ut "Tallet er ikke mellom 0 og 9. Prøv på nytt!"
    hent nytt tall fra brukeren og legg inn i variabelen `tall`
    gå tilbake til "så lenge" og sjekk det boolske uttrykket på nytt
skriv ut "Tusen takk!"
```

En `while`-setning er en slags blanding mellom en `if`-setning og en `for`-løkke. Den er akkurat som `if` med den ene forskjellen er at man går tilbake og tester om det boolske uttrykket er sant en gang til. Hvis det er sant, så utfører vi instruksjonene under en gang til, hvis ikke så avsluttes løkka.

## Funksjoner

Noen ganger ønsker vi å bruke samme kode på flere steder i et program. La oss si at vi flere ganger i løpet av et program ønsker å spørre brukeren om et tall mellom 0 og 9. Da kan vi bruke koden i forrige eksempel som utgangspunkt og putte dette inn i en **funksjon**.

```python
def hent_et_tall():
    tall = input("Skriv inn et tall mellom 0 og 9: ")
    while int(tall) < 0 or int(tall) > 9:
        print("Tallet er ikke mellom 0 og 9. Prøv på nytt!")
        tall = input("Skriv inn et tall: ")
    return tall

valg = hent_et_tall()
print(valg)
```

```
lag en funksjon med navn "hent_et_tall" som skal gjøre følgende:
    hent et tall fra brukeren og legg i variabelen `tall`
    så lenge variabelen `tall` er mindre enn 0 eller større enn 9, utfør instruksjonene under:
        skriv ut "Tallet er ikke mellom 0 og 9. Prøv på nytt!"
        hent nytt tall fra brukeren og legg inn i variabelen `tall`
        gå tilbake til "så lenge" og sjekk det boolske uttrykket på nytt
    returner innholdet av variabelen `tall`
  
kjør funksjonen hent_et_tall og legg det returnerte tallet i variabelen valg  
skriv ut innholdet av variabelen valg
```

De fleste funksjoner *returnerer* en verdi, selv om de ikke må det. Funksjonen over returnerer hvilket tall brukeren har valgt. I forrige leksjon brukte vi for eksempel noen strengfunksjoner som returnerte verdier. For eksempel returnerer `upper("Hei")` verdien `"HEI"`:

```python
tekst = upper("Hei")
print(tekst)
```
```
HEI
```

En funksjon kan også ta imot **argumenter** (eller *parametre*). I eksempelet over tar funksjonen inn en streng som parameter og returnerer en annen. I eksempelet under tar funksjonen to parametre, begge tall, som definerer øvre og nedre grense for hvilket tall brukeren kan velge:

```python
def hent_et_tall(min_tall, maks_tall):
    tall = input("Skriv inn et tall mellom " + str(min_tall) + " og " + str(maks_tall) + ": ")
    while int(tall) < min_tall or int(tall) > maks_tall:
        print("Tallet er for stort eller for lite.")
        tall = input("Skriv inn et tall: ")
    return int(tall)

print(hent_et_tall(0, 9))
```

Navnekonvensjonen i Python er slik at man helst bruker `_`, altså "underscore" for å skille mellom flere ord i et funksjonsnavn. Det er også mulig å bruke *camelCase* slik man helst gjør i programmeringsspråket Java.

## Auto completion

Vi kan bruke en IDE, som Thonny, til å hjelpe oss og skrive kode raskere og riktigere. En teknikk vi kan bruke i de fleste IDE'er er *auto completion*. Hvis vi har funksjonen `hent_et_tall()` i modulen `brukerinput` og vi står i en annen fil og vil bruke modulen, kan vi trykke på `Ctrl` + `Mellomrom` etter vi har skrevet `brukerinput.` og da vil alle funksjonene i modulen komme opp. Hvis vi i tillegg hadde begynt å skrive funksjonsnavnet, ville kun de som matchet komme opp i listen:

![](images/Thonny15.png)

Dette gjelder også variabler. I eksempelet under kommer både den innebygde funksjonen `len()` og variabelen `lengde` opp når vi trykker `Ctrl` + `Mellomrom` etter + ha skrevet `le`. 

![](images/Thonny16.png)


<a name="oppgaver3"/>
## Oppgave

* Lag et program som lar brukeren skrive inn flere positive tall. 
* Alle disse tallene skal puttes inn i en liste (husk å konvertere input fra tekst til tall). 
* Når brukeren legger inn -1, skal programmet slutte å spørre brukeren om flere tall.
* Tilslutt skal programmet skrive ut hvor mange tall brukeren har skrevet inn.

# Bruke biblioteker

I tidligerer leksjoner har vi brukt bla *math*-biblioteket. Dette er innebygd i Python og vi trengte ikke gjøre noe annet enn å importere det i programfilen. Nå skal vi bruke et bibliotek som ikke er innebygd og vi må derfor legge det til som en pakke i IDE'en vår, Thonny.

Velg *Tools/Manage packages...* fra menyen og installer pakken **matplotlib**.

Hvis installasjonen er vellykket skal `matplotlib` komme opp i listen over installerte pakker:

![](images/Thonny18.png)

Nå er vi klare til å bruke biblioteket. La oss lage et stolpediagram:

```python
import matplotlib.pyplot as plt
import numpy as np

def show_histogram(counts, labels, ylabel, xlabel):
    x = np.arange(len(counts))
    plt.bar(x, height=counts)
    plt.xticks(x, labels);
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.show()
    
# Test funksjon
show_histogram([2, 16, 3, 1, 4.5], ["a", "b", "c", "d", "e"], "Antall", xlabel = "Kategori")
```

Dette gjør at følgende vindu popper opp:

![](images/Thonny19.png)


