# Innhold

*  [If-Else](#if-else)
*  [Elif](#elif)
*  [Innrykk](#innrykk)
*  [For-løkker](#for-løkker)
*  [While-løkker](#while-løkker)
*  [Funksjoner](#funksjoner)
*  [Flere programfiler](#flere-programfiler)
*  [Eksempelprogram](#eksempelprogram)
*  [Auto completion](#auto-completion)
*  [Oppgaver](#oppgaver)
 

I denne leksjonen skal vi lære om kontrollflyt, rekkefølgen på instruksjonene i et program og hvordan vi kan endre denne utifra hva som skjer i programmet. I forrige leksjon snakket vi om boolske variabler og logiske uttrykk. Det er nettopp slike vi skal bruke for å ta avgjørelser underveis i et program.

# If-Else

Den mest klassiske måten å ta en avgjørelse i et dataprogram på, er å bruke instruksjonen `if`. De aller fleste programmeringsspråk har denne instruksjonen. Med pseudokode kan vi si det slik:

```
hvis noe er sant så:
    gjør noe
ellers:
    gjør noe annet
```

Vi kan prøve oss på et Python-program:

```python
tall = input("Skriv inn et tall: ")
tall = int(tall)
if tall < 5:
    print("Tallet er mindre enn 5")
else:
    print("Tallet er ikke mindre enn 5")
```

Her lar vi først brukeren skrive inn et tall, så konverterer vi input'en fra tekst til et tall. Deretter sier vi at hvis tallet i variabelen `tall` er mindre enn 5 så skal vi utføre instruksjonen på linja rett under og skrive ut teksten `"Tallet er mindre enn 5"`. Hvis ikke, altså hvis `tall` er 5 eller større, så skal vi skrive ut teksten `"Tallet er ikke mindre enn 5"`.

# Elif

`elif` er en kortversjon av *else if*. Vi kan bruke dette for å lage flere alternativer i samme *if*-setning:

```python
tall = input("Skriv inn et tall: ")
tall = int(tall)
if tall < 5:
    print("Tallet er mindre enn 5")
elif tall == 5:
    print("Tallet er 5")
elif tall == 6:
    print("Tallet er 6")
else:
    print("Tallet er større enn 6")
```

Dette kan uttrykkes med pseudokode på følgende måte:

```
hvis variabelen tall er mindre enn 5 så:
    skriv ut "Tallet er mindre enn 5"
ellers, hvis variabelen tall er lik 5 så:
    skriv ut "Tallet er 5"
ellers, hvis variabelen tall er lik 6 så:
    skriv ut "Tallet er 6"
ellers, hvis ingen av uttrykkene over er sann, så:
    skriv ut "Tallet er større enn 6"
```

Vi kan bruke så samme mange `elif` vi vil inne i en *if*-setning.


# Innrykk

Når vi skal utføre en eller flere instruksjoner som et resultat av en avgjørelse, for eksempel ved bruk av en `if`-setning, så bruker vi *innrykk* foran denne/disse instruksjonen(e). Et innrykk kan være ett eller flere mellomrom.

Det er viktig å bruke **samme** innrykk i et program. Dette gjør at koden blir mer leselig, og det blir lettere å unngå feil. Bruk for eksempel alltid 4 mellomrom for innrykk, eller ett trykk på `tab`-knappen.

I eksempelet i forrige avsnitt utførte vi kun *en* instruksjon etter `if` eller `elif` eller `else`. Men her kan vi like gjerne utføre flere instruksjoner. For å få til dette må disse instruksjonene ha samme innrykk. La oss se på følgende eksempel:

```python
tall = input("Skriv inn et tall: ")
tall = int(tall)
if tall<5:
    print("Tallet er mindre enn 5")
    print("Kanskje du skal prøve et større tall")
else:
    print("Tallet er ikke mindre enn 5")
    print("Det betyr at enten er tallet 5")
    print("eller så er det større")
print("Nå er vi ferdig")
```

* Her sier vi at hvis variabelen `tall` er mindre enn 5, så skal instruksjonene på de **to** neste linjene utføres, fordi disse to instruksjonene bruker samme innrykk.
* Hvis ikke, altså hvis tallet er 5 eller større så skal instruksjonene på de tre linjene etter `else:` utføres.
* MEN, instruksjonen på den aller siste linjen skal utføres uansett, både hvis tallet er mindre enn 5 eller ikke.

Eksperimenter ved å kjøre dette programmet noen ganger, med forskjellig innrykk på de forskjellige linjene.

# For-løkker

Vi lagde løkker allerede i introduksjonsleksjonen. La oss se på samme eksempel en gang til:

```python
for i in range(0, 3):
    print("Hei verden!")
```
```
Hei verden!
Hei verden!
Hei verden!
```

* Vi lager en variabel, `i`, som skal være 0 når vi starter løkka.
* Så utfører vi instruksjonene på linjen(e) etter kolonet. 
* Deretter øker vi verdien av `i` med 1 og sjekker om `i` fremdeles er mindre enn 3.
* Hvis den er det så utfører vi instruksjonen(e) en gang til, øker `i` med 1 og sjekker en gang til.
* Hvis ikke avsluttes løkka

Vi kan også bruke variabler. Følgende er i praksis helt likt det forrige:

```python
start = 0
slutt = 3
for i in range(start, slutt):
    print("Hei verden!")
```

Vi trenger ikke starte med 0:

```python
for i in range (2, 5):
    print(i)
```
```
2
3
4
```

Vi kan også telle nedover ved å legge på et tredje argument, `-1`:

```python
for i in range (5, 2, -1):  # Starter på 5, holder på til i er 2,
    print(i)
```
```
5
4
2
```

Og vi trenger ikke øke eller minske med 1:

```python
for i in range (2, 6, 2):  # Starter på 2, holder på til i er 6, øker med 2
    print(i)
```
```
2
4
```

På samme måte som i avsnittet om `if`, kan vi utføre *flere* enn en instruksjon i løkka ved å bruke innrykk:

```python
for i in range (0, 2):
    print("Inne i løkka 1")
    print("Inne i løkka 2")
print("Ferdig med løkka")
```
```
Inne i løkka 1
Inne i løkka 2
Inne i løkka 1
Inne i løkka 2
Ferdig med løkka
```

## For-løkke med lister

Det finnes en veldig enkel metode for å traverse lister:

```python
liste = ["Fredrik", "Arne", "Karl", "Åge"]
for navn in liste:
    print(navn)
```
```
Fredrik
Arne
Karl
Åge
```

Med pseudokode bli dette:
```
for hvert navn i lista:
    skriv ut navnet
```

Det som skjer her er at vi lager en løkke som som itererer like mange ganger som det er elementer i lista. I tillegg lager vi en variabel `navn` som får verdien av det elementet i lista man er på. 

# While-løkker

For-løkker brukes når vi skal gjøre noe et bestemt antall ganger. Men noen ganger vet vi ikke på forhånd hvor mange ganger vi skal utføre noe. Da kan vi si at vi skal utføre noe *så lenge noe er sant*. Til det bruker vi instruksjonen *while*:

```python
tall = input("Skriv inn et tall mellom 0 og 9: ")
while int(tall) < 0 or int(tall) > 9:
    print("Tallet er ikke mellom 0 og 9. Prøv på nytt!")
    tall = input("Skriv inn et tall: ")
print("Tusen takk!")
```

Vi kan uttrykke dette med pseudokode:

```
så lenge variabelen `tall` er mindre enn 0 eller større enn 9, utfør instruksjonene under:
    skriv ut "Tallet er ikke mellom 0 og 9. Prøv på nytt!"
    hent nytt tall fra brukeren og legg inn i variabelen `tall`
    gå tilbake til "så lenge" og sjekk det boolske uttrykket på nytt
skriv ut "Tusen takk!"
```

En `while`-setning er en slags blanding mellom en `if`-setning og en `for`-løkke. Den er akkurat som `if` med den ene forskjellen er at man går tilbake og tester om det boolske uttrykket er sant en gang til. Hvis det er sant, så utfører vi instruksjonene under en gang til, hvis ikke så avsluttes løkka.

# Funksjoner

Noen ganger ønsker vi å bruke samme kode på flere steder i et program. La oss si at vi flere ganger i løpet av et program ønsker å spørre brukeren om et tall mellom 0 og 9. Da kan vi bruke koden i forrige eksempel som utgangspunkt og putte dette inn i en **funksjon**.

```python
def hent_et_tall():
    tall = input("Skriv inn et tall mellom 0 og 9: ")
    while int(tall) < 0 or int(tall) > 9:
        print("Tallet er ikke mellom 0 og 9. Prøv på nytt!")
        tall = input("Skriv inn et tall: ")
    return tall

valg = hent_et_tall()
print(valg)
```

```
lag en funksjon med navn "hent_et_tall" som skal gjøre følgende:
    hent et tall fra brukeren og legg i variabelen `tall`
    så lenge variabelen `tall` er mindre enn 0 eller større enn 9, utfør instruksjonene under:
        skriv ut "Tallet er ikke mellom 0 og 9. Prøv på nytt!"
        hent nytt tall fra brukeren og legg inn i variabelen `tall`
        gå tilbake til "så lenge" og sjekk det boolske uttrykket på nytt
    returner innholdet av variabelen `tall`
  
kjør funksjonen hent_et_tall og legg det returnerte tallet i variabelen valg  
skriv ut innholdet av variabelen valg
```

De fleste funksjoner *returnerer* en verdi, selv om de ikke må det. Funksjonen over returnerer hvilket tall brukeren har valgt. I forrige leksjon brukte vi for eksempel noen strengfunksjoner som returnerte verdier. For eksempel returnerer `upper("Hei")` verdien `"HEI"`:

```python
tekst = upper("Hei")
print(tekst)
```
```
HEI
```

En funksjon kan også ta imot **argumenter** (eller *parametre*). I eksempelet over tar funksjonen inn en streng som parameter og returnerer en annen. I eksempelet under tar funksjonen to parametre, begge tall, som definerer øvre og nedre grense for hvilket tall brukeren kan velge:

```python
def hent_et_tall(min_tall, maks_tall):
    tall = input("Skriv inn et tall mellom " + str(min_tall) + " og " + str(maks_tall) + ": ")
    while int(tall) < min_tall or int(tall) > maks_tall:
        print("Tallet er for stort eller for lite.")
        tall = input("Skriv inn et tall: ")
    return int(tall)

print(hent_et_tall(0, 9))
```

Navnekonvensjonen i Python er slik at man helst bruker `_`, altså "underscore" for å skille mellom flere ord i et funksjonsnavn. Det er også mulig å bruke *camelCase* slik man helst gjør i programmeringsspråket Java.

# Flere programfiler

Når programmene våre begynner å bli store bør vi putte koden i flere forskjellige programfiler. Da er det lurt å putte kode som hører bra sammen i samme programfil. På denne måten holder vi orden i koden og gjør den lettere å lese. Dette er som nevnt tidligere viktig hvis noen andre enn oss selv skal kunne forstå og bygge videre på kode vi har laget.

La oss putte funksjonen vi lagde i fila *brukerinput.py*:

*brukerinput.py:*
```python
def hent_et_tall(min_tall, maks_tall):
    tall = input("Skriv inn et tall mellom " + str(min_tall) + " og " + str(maks_tall) + ": ")
    while int(tall) < min_tall or int(tall) > maks_tall:
        print("Tallet er for stort eller for lite.")
        tall = input("Skriv inn et tall: ")
    return int(tall)
```

For å bruke denne koden i en annen fil må vi importere modulen "brukerinput" og prefikse funksjonen med navnet på modulen. La oss putte dette i programfilen *mitt_program.py*:

*mitt_program.py:*
```python
import brukerinput

print(brukerinput.hent_et_tall(0, 9))
```

Dette er akkurat slik vi gjorde det når vi brukte *math*-biblioteket i forrige leksjon. Det å lage funksjoner og **gjenbrukbar** kode er altså det samme som å lage biblioteker. På denne måten kan vi bygge opp funksjonalitet som vi selv trenger flere ganger, eller vi kan bygge opp biblioteker som også andre kan benytte seg av.

# Auto completion

Vi kan bruke en IDE, som Thonny, til å hjelpe oss og skrive kode raskere og riktigere. En teknikk vi kan bruke i de fleste IDE'er er *auto completion*. Hvis vi har funksjonen `hent_et_tall()` i modulen `brukerinput` og vi står i en annen fil og vil bruke modulen, kan vi trykke på `Ctrl` + `Mellomrom` etter vi har skrevet `brukerinput.` og da vil alle funksjonene i modulen komme opp. Hvis vi i tillegg hadde begynt å skrive funksjonsnavnet, ville kun de som matchet komme opp i listen:

![](images/Thonny15.png)

Dette gjelder også variabler. I eksempelet under kommer både den innebygde funksjonen `len()` og variabelen `lengde` opp når vi trykker `Ctrl` + `Mellomrom` etter + ha skrevet `le`. 

![](images/Thonny16.png)

# Eksempelprogram

La oss benytte oss av flere av konseptene vi har lært i denne leksjonen for å lage et større program.

La oss lage en funksjon som lar brukeren velge mellom addisjon, subtraksjon, multiplikasjon og divisjon.

```python
def velg_operasjon():
    valg = input("Velg regnemåte\n 1. Addisjon\n 2. Subtraksjon\n 3. Multiplikasjon\n 4. Divisjon\n")
    while int(valg) < 1 or int(valg) > 4:
        print("Ugyldig menyvalg!")
        valg = input("Velg regnemåte\n 1. Addisjon\n 2. Subtraksjon\n 3. Multiplikasjon\n 4. Divisjon\n")
    return int(valg)
```

La oss i tillegg lage en funksjon som utfører regneoperasjonen, med to tall og gitt regneoperasjon:

```python
def utregning(tall1, tall2, operasjon):
    if operasjon == 1:
        resultat = tall1 + tall2
    elif operasjon == 2:
        resultat = tall1 - tall2
    elif operasjon == 3:
        resultat = tall1 * tall2
    else:
        resultat = tall1 / tall2
    return resultat
```

Så kan vi skrive kode som bruker begge disse funksjonene for å lage en "kalkulator":

```python
tall1 = hent_et_tall(0, 9)
tall2 = hent_et_tall(0, 9)
operasjon = velg_operasjon()
resultat = utregning(tall1, tall2, operasjon)
print("Svaret er : " + str(resultat))
```

# Oppgaver

## Oppgave 1

* Lag et program som lar brukeren skrive inn flere positive tall. 
* Alle disse tallene skal puttes inn i en liste (husk å konvertere input fra tekst til tall). 
* Når brukeren legger inn -1, skal programmet slutte å spørre brukeren om flere tall.
* Tilslutt skal programmet skrive ut hvor mange tall brukeren har skrevet inn.

## Oppgave 2

* Lag en ny kodefil som du kaller *statistikk.py*.
* Lag to funksjoner i denne kodefila:
  * En funksjon som returnerer summen av alle tallene i en liste. Listen skal sendes inn til funksjonen som et argument.
  * En funksjon som returnerer gjennomsnittet av alle tallene i en liste. Listen skal sendes inn til funksjonen som et argument.
* Utvid så funksjonaliteten i oppgave 1 slik at den i tillegg skriver ut både summen av tallene og gjennomsnittet, ve3d å bruke de nye funksjononene du har laget.

