# Innhold

*  [IDE](#ide)
*  [Hello World](#hello-world)
*  [Instruksjoner](#instruksjoner)
*  [Kodefeil](#kodefeil)
*  [Løkker](#løkker)
*  [Variabler](#variabler)
*  [Debugging](#debugging)
*  [Input](#input)
*  [Oppgaver](#oppgaver)

I denne leksjonen skal vi begynne å skrive enkle Python-programmer. Vi går relativt fort igjennom flere konsepter uten å gå i dybden. Vi vil gå igjennom alt i større detalj senere, så det er ikke så farlig om du ikke forstår alt med en gang. Nå skal vi bare komme igang med å skrive kode.

# IDE
Man kan skrive kode med en enkel teksteditor, men det er lettere å bruke et program som er spesiallaget til dette formålet. Derfor bruker vi gjerne en *IDE* (Integrated Development Environment). Det finnes flere IDE'er som er brukbare for hvert språk, og noen IDE'er kan brukes til flere programmeringsspråk. Vi skal bruke en enkel IDE som er spesiallaget for Python-undervisning. Denne IDE'en heter "Thonny" og vi kan laste den ned herfra: [http://thonny.org](http://thonny.org). Hvis noen ønsker å bruke en mer avansert IDE er det mulig å bruke andre, f.eks [PyDev](http://www.pydev.org/),
 men jeg vil anbefale å bruke Thonny da det er denne som brukes i undervisningen.

Last ned Thonny og følg instruksjonene på nettsiden for å installere den på din maskin. Når du så starter Thonny vil du få opp et vindu som ser slik ut (Windows):

![](images/Thonny1.png)


Dette vinduet har 3 ruter. I ruten øverst til venstre skriver vi kode, i ruten under ser vi resultatet når vi kjører koden, og ruten til høyre brukes for "debugging". Vi skal se litt på debugging tilslutt i denne leksjonen. 

Hvis du ikke ser "Variables"-vinduet kan du velge `View/Variables` fra menyen.

# Hello World

Nå kan vi lage vårt første program i Python. Skriv følgende kode i ruten øverst til venstre:

```python
print("Hei verden!")
```

Dette er et enkelt men fullverdig Python-program hvor vi ganske enkelt sier at programmet skal skrive ut teksten "Hei verden!". For at vi skal kunne kjøre programmet må vi først lagre det i en fil. Velg *File/Save* eller trykk på diskett-knappen, velg en passende mappe og bruk f.eks filnavnet *hello.py*. Vi bruker alltid filetternavnet *.py* for Python-programmer.

Kjør programmet ved å trykke på den grønne start-knappen på verktøylinja. Da vil du se at teksten "Hei verden!" blir skrevet ut i ruten nederst til venstre:

![](images/Thonny3.png)

Merk at koden i Thonny blir fargelagt etter visse regler. Dette kalles "syntax highlighting" og gjør at det blir lettere å lese koden.

# Instruksjoner

Et program består av en serie med instruksjoner (eller kommandoer). Vi forteller Python hvordan en oppgave skal utføres ved å gi den en rekke instruksjoner, litt på samme måte som en matoppskrift gir oss instruksjoner om hvordan vi skal lage en rett.

I den virkelige verden kunne man ha laget instruksjoner for å betale i en selvbetjent kasse i butikken:

```
Så lenge det er varer i handlevogna:
    ta ut en vare
    scan strekkode
trykk på "Ferdig"
utfør betaling
```

Dette er ikke et Python-program men bruker samme oppbygning og kalles gjerne *pseudo-kode*. For Python og andre programmeringsspråk er det mer eller mindre strenge regler for hvordan man skal skrive koden.

# Kodefeil

Hvis vi lager feil i koden får vi feilmeldinger fra Python. Vi kan for eksempel skrive `printt` istedetfor `print`. `printt` er ikke en instruksjon Python kjenner til og derfor vil vi få en feilmelding:

![](images/Thonny3-2.png)

Når vi skriver ut tekst som vi gjør her bruker vi en tekststreng, `"Hei Verden!"`. En tekststreng må alltid ha et anførselstegn på hver side. Prøv å ta bort det ene og se hva som skjer da.

# Løkker

La oss prøve å gjøre noen endringer på programmet vi har skrevet. Hvordan kan vi f.eks skrive ut teksten "Hei verden!" flere ganger? Den aller enkleste løsningen er å endre programmet slik at vi skriver samme `print`-kommando flere ganger:

![](images/Thonny4.png)

Men det finnes mer elegante måter å gjøre dette på. Vi kan heller lage en **løkke**:

```python
for i in range(0, 3):
    print("Hei verden!")
```

Dette gir akkurat samme resultat som før. 

I dette eksempelet introduserte vi to nye konsepter, *variabler* og *løkker*. Vi går ikke i dybden på dette nå, men kommer tilbake til det i senere leksjoner. Likevel, med pseudokode kan vi si det på denne måten:

```{python, eval = FALSE}
utfør 3 ganger:
    print("Hei verden!")
```

Men vi introduserte også variabelen `i` som inneholder et tall. En litt mer nøyaktig beskrivelse av hva som skjer kan være slik:

```{python, eval = FALSE}
utfør 3 ganger, først med i=0, så med i=1, så med i=2:
    print("Hei verden!")
```

For å se at verdien på `i` endrer seg kan vi også skrive ut verdien av variabelen i koden. Legg til kommandoen `print(i)` på denne måten:

```python
for i in range(0, 3):
    print("Hei verden!")
    print(i)
```

Siden begge `print`-kommandoene er har samme innrykk, dvs at det er samme antall mellomrom foran kommandoen, vil begge `print`-kommandoene bli utført som en del av løkka.

Når vi kjører programmet ser vi dette:

![](images/Thonny5.png)

Første gang teksten skrives ut er `i` 0, så 1, så 2. Merk at den blir aldri 3. Når vi lager løkken sier vi at `i` først skal være 0, så skal den økes med **en** for hver gang, så lenge den **er mindre** enn 3.

Vi kan endre innrykket slik at den andre `print`-kommandoen ikke er en del av løkka. Her utføres den andre kommandoen etter at løkka er ferdig:

![](images/Thonny5-2.png)

# Variabler

Vi brukte variabelen `i` for å lage en løkke, men vi kan bruke variabler på flere måter, for eksempel til å utføre enkel regning:

![](images/Thonny6.png)

Her setter vi først variabelen `a` til 5, så setter vi variabelen `b` til 6. Deretter setter vi variabelen `c` lik `a + b` som da i praksis blir `11`. På neste linje setter vi `c` lik `c + 2`. Vi skal altså øke verdien av `c` med 2. Siden `c` er 11 før denne kommandoen, blir `c` 13 etter kommandoen er utført. På siste linje skriver vi ut verdien av `c` som nå er 13.

Merk innholdet i ruten til høyre. Denne viser nå verdien av alle variablene slik de var på slutten av eksekveringen (kjøring) av koden. Vi kan også bruke denne ruten til å vise verdien av variablene mens vi kjører. Dette kaller vi "debugging".

# Debugging

Debugging er egentlig prosessen å fjerne "bugs" eller feil fra et program, men brukes mer spesifikt om et spesielt nyttig verktøy for å få til dette, nemlig det å kjøre et program linje for linje, mens vi kan inspisere innholdet i variablene.

La oss prøve dette men en gang. Trykk på insekt-knappen (bug'en) ved siden av start-knappen. Thonny vil da se slik ut:

![](images/Thonny7.png)

Nå har vi startet programmet, men det venter på videre instruksjoner fra oss. Første kommando har enda ikke blitt kjørt. La oss nå kjøre kommandoen på den første linja. Det gjør vi ved å trykke på ikonet til høyre for insektet (step over):

![](images/Thonny8.png)

Nå er første kommando kjørt. Denne kommandoen sa at vi skulle gi variabelen `a` verdien 5. Dette kan vi nå verifisere ved å inspisere variabel-vinduet til høyre. 

La oss også kjøre neste kommando også ved å trykke på "step over" en gang til:

![](images/Thonny9.png)

Da ser vi at variabelen `b` har fått verdien 6. 

Vi kjører enda en kommando:

![](images/Thonny10.png)

Nå har også `c` fått en verdi. Denne gangen brukte vi ikke en enkel tilordning av verdi, men vi utførte en enkel addisjon. 

Vi kjører neste kommando også:

![](images/Thonny11.png)

Nå sa vi at verdien av `c` skulle bli `c + 2`, altså vi legger til 2 på eksisterende verdi. Dette gjenspeiles i variabelvinduet som nå viser at `c` er 13. 

Vi kjører siste kommando:

![](images/Thonny12.png)

Nå ble ingen av variablene endret, men vi ser i "Shell"-ruten at verdien av `c` har blitt skrevet ut.

Dette var et enkelt eksempel. Når vi etterhvert lager litt større programmer er dette et kjempenyttig verktøy vi kan bruke for å forstå hvordan programmet vårt fungerer.

# Input

For å lage et interaktivt program kan vi spørre brukeren om input:

![](images/Thonny13.png)

Kommandoen på den første linjen sier at vi skal skrive ut teksten `"Skriv inn et tall: "` og deretter vente på at brukeren skriver inn et tall. Når brukeren har skrevet tallet og trykket på "Enter" vil variabelen `a` ha verdien som brukeren skrev inn.

![](images/Thonny14.png)

# Oppgaver

Nå skal dere prøve å løse noen konkrete oppgaver. Bruk kodeeksemplene i leksjonen som utgangspunkt.

## Oppgave 1

Lag et program som starter slik:

```python
a = 3
b = 2
```

Deretter skal det gjøre følgende: 
* La `c`  bli `a` ganger `2`
* La `d` bli `b` pluss `c`
* Gjør verdien av `d` til det dobbelte av hva den er
* Skriv ut verdien av `d`

Er verdien av `d` `16`?

Hvis ikke må du bruke debuggeren til å finne ut hvorfor.

## Oppgave 2

Nå skal du utvide programmet fra oppgave 1.

Legg til denne linjen på slutten:


```python
e = input("Skriv inn enn tekst: ")
```

Deretter skal programmet skrive ut verdien av `e` like mange ganger som verdien av `d`.

Kjør programmet med debuggeren og observer hvordan løkketelleren øker underveis.