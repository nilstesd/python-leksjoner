# Introduksjon til Python-programmering

## Om kurset

Dette er et introduksjonskurs både til generell programmering og til programmeringsspråket Python. Målet er at dere skal lære de viktigste konseptene innen programmering, slik at dere forstår hva programmering er og at dere har noe å bygge videre på hvis dere ønsker å lære mer om programmering.

Python er et språk som er godt egnet for undervisning da det er relativt lett å lære samtidig som det er et rikt programmeringsspråk. Språket har blitt mer og mer populært og brukes for eksempel ofte til dataanalyse i "Big Data"-løsninger.

I Python er det et ganske stort skille mellom versjon 2 og 3 av språket. Dette kurset er basert på versjon 3 og derfor vil noe av koden som presenteres i kurset ikke fungere hvis man bruker versjon 2.

Når dere går igjennom leksjonene bør dere prøve all koden som står i leksjonen selv også. I tillegg bør dere eksperimentere ved å endre litt på koden og observere hva som skjer. På denne måten vil dere få "hands-on" erfaring med koding og det blir mye lettere å lære og huske konseptene. 

## Leksjoner

* UKE 1: [Introduksjon](introduksjon.md)
* UKE 2: [Datatyper](datatyper.md)
* UKE 3: [Kontrollflyt](kontrollflyt.md)
* UKE 4: [Databaser](databaser.md)
* UKE 5: [GUI](gui.md)