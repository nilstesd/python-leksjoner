# Innhold

*  [Kommentarer](#kommentarer)
*  [Variabler](#variabler)
*  [Datatyper](#datatyper)
*  [Streng](#streng)
*  [Tall](#tall)
*  [Boolean](#boolean)
*  [Liste](#liste)
*  [Oppgaver](#oppgaver)

I denne leksjonen skal vi se mer på variabler. Vi skal se at en variabel har en *datatype* og vi skal utføre aritmetikk og andre operasjoner med variablene.

# Kommentarer

Før vi begynner med variabler og datatyper skal vi se på *kommentarer*. Kommentarer gjør ingenting i selve koden, men vi kan bruke dem for å forklare hva som skjer i koden. Da kan koden bli mer lesbar og det da blir lettere for andre å forstå koden. Dette blir spesielt viktig når programmet blir stort og uoversiktlig. Vi starter kommentarer med tegnet `#`:

```python
# Lag variabler for bredde og lengde
bredde = 10
lengde = 20

# Utfør regneoperasjon med begge tallene
areal = bredde * lengde

# Skriv ut resultatet
print("Arealet er : " + str(areal))
```

I tillegg til å skrive kommentarer på hele linjer som i eksempelet over, kan vi legge inn kommentarer på slutten av vanlige kodelinjer:

```python
bredde = 15  # Bredden på rektangelet
lengde = 10  # Lengden på rektangelet
```

# Variabler

Vi bruker variabler for å lagre verdier. Dere husker kanskje variabler fra matematikken?

I følgende eksempel sier vi at variabelen `a` skal ha verdien 5.

```python
a = 5
```

I navnet *variabel* ligger det at verdien kan endres. Det kan vi gjøre på følgende måte:

```python
a = a + 5
```

Hvis `a` var 5 i utgangspunktet, vil `a` bli 10 etter å har kjørt denne instruksjonen.

Ofte bruker man enkle navn på variabler, som `a` eller `b`, men det er egentlig en bedre praksis å bruke skikkelige navn. Si at vi skal regne ut arealet på et rektangel. Vi kan da introdusere to variabler, `lengde` og `bredde`:

```python
lengde = 10
bredde = 2
areal = lengde * bredde
```

Koden over er lettere å forstå enn om vi hadde brukt navnene `a` og `b`. Da blir det også lettere å skrive kode uten feil, og det blir lettere for andre å lese koden. Når man programmerer gjør man gjerne det i team, og da er det kjempeviktig at kode er lettlest sånn at alle på teamet kan forstå koden. Da kan de andre også endre denne koden uten stor fare for å innføre feil.

# Datatyper

Alle variablene vi har brukt til nå har representert tall, men de kan også representere andre *typer* data. Datatyper er sentralt i alle programmeringsspråk, men man håndterer de på forskjellig måte. I Python trenger man ikke si hvilken type en variabel skal ha, man gir bare variablene verdier direkte. Python holder likevel orden på hvilken datatype en variabel har.

# Streng

La oss først se på *strenger* som vi bruker for å representere tekst. Strenger har alltid anførseltegn på hver side teksten. Her sier vi at variabelen `melding` skal inneholde teksten `"Hei verden"`, så skal vi skrive ut verdien av variabelen: 

```python
melding = "Hei verden!"
print(melding)
```

Dette gir utskriften:

```
Hei verden!
```

På samme måte som denne litt enklere koden:

```python
print("Hei verden!")
```

## Konkatenering

Konkatenering betyr å legge sammen strenger. Dette gjør vi enkelt i Python ved å bruke `+`-operatoren:

```python
melding = "Hei"
melding = melding + " "
melding = melding + "Ola"
melding = melding + "!"
print(melding)
```

Gir utskriften:

```
Hei Ola!
```

Vi kan også legge sammen strenger og tall, men da må vi gjøre noe ekstra. Følgende kode gir oss feilmelding fordi det i utgangspunktet ikke er lov å legge sammen en streng og et tall:

```python
egg = 5
melding = "Du har " + egg + " i kurven"
print(melding)
```

Hvis vi skal gjøre dette riktig må vi konvertere tallet til en streng først. Det kan vi gjøre med funksjonen `str()` på denne måten:

```python
egg = 5
egg = str(egg)
melding = "Du har " + egg + " i kurven"
print(melding)
```

På den andre linja endrer vi datatypen slik at `egg` inneholder strengen `"5"` istedetfor tallet `5`. Når vi konkatenerer på neste linje er det bare strenger som legges sammen. Vi kan også gjøre dette på en litt raskere måte og konvertere akkurat der vi trenger det:

```python
egg = 5
melding = "Du har " + str(egg) + " i kurven"
print(melding)
```

Begge gir utskriften:

```
Du har 5 egg i kurven
```

## Strengfunksjoner

I forrige eksempel brukte vi funksjonen `str()` som er innebygd i Python. Vi skal se mer på funksjoner i en senere leksjon, men følgende er 3 innebygde funksjoner for behandling av strenger:

```python
egg = 5
melding = "Du har " + str(egg) + " egg i kurven"
print(melding.upper())
print(melding.lower())
print(len(melding))
```

Dette gir følgende utskrift:

```
DU HAR 5 EGG I KURVEN
du har 5 egg i kurven
21
```

* `upper()` konverterer alle små bokstaver i strengen til store bokstaver.
* `lower()` konverterer alle store bokstaver i strengen til små bokstaver.
* `len()` gir lengden av strengen (antall tegn) som et tall.

I koden er det en ting som er verdt å merke seg og som kan være forvirrende. For å bruke funksjonen `len()` putter vi strengen vi skal finne lengden på inn i parantesene. For å bruke `upper()` og `lower()` slenger vi disse bakpå variabelen med et punktum imellom. 
Dette er fordi `len()` (og også `str()`) er en innebygd funksjon, mens `upper()` og `lower()` egentlig er *metoder* på streng-objektet. Dere trenger *ikke* å bry dere om denne distinksjonen da dette er utefor skopet til dette kurset og dere kan heretter bare referere til begge som *funksjoner*.

Vi kan også trekke ut delstrenger fra en streng. Se følgende eksempel:

```python
egg = 5
melding = "Du har " + str(egg) + " egg i kurven"
print(melding[0:2])
print(melding[3:6])
```

Dette gir følgende utskrift:

```
Du
har
```

`melding[0,2]` plukker ut tegnene fra plassering 0 (aller først i strengen) til men ikke inkludert plassering 2. Da får vi med oss tegnene `D` på plassering 0 og `u` på plassering 1. `melding[3:6]` tar med tegnene fra plassering 3, 4 og 5 som gir `"har"`.

Python har langt flere innebygde strengfunksjoner. Prøv f.eks. å Google "python strings" og dere vil finne mye materiale på dette.

# Tall

Forskjellige programmeringsspråk kan ha forskjellige datatyper for tall, men alle skiller mellom heltall og desimaltall. I Python har vi to typer:

* int (Integer) for *heltall*.
* float (Floating point) for *desimaltall*.

Legg merke til at vi bruker `.` som desimalskilletegn og ikke `,`. Dette er vanlig i de fleste programmeringsspråk.

La oss se på følgende kode:

```python
a = 5
b = 6.5
c = a + b
print(a)
print(type(a))
print(b)
print(type(b))
print(c)
print(type(c))
```

Dette gir følgende utskrift:

```
5
<class 'int'>
6.5
<class 'float'>
11.5
<class 'float'>
```

Vi bruker her funksjonen `type()` som er innebygd i Python og gir oss datatypen til en variabel.

Her er det flere ting å merke seg i koden:
* Datatypen til `a` bestemmes av at vi gir den verdien `5`, som er et heltall, og datatypen blir derfor en *int*.
* Datatypen til `b` bestemmes av at vi gir den verdien `6.5`, som er et desimaltall, og datatypen blir derfor en *float*.
* Det er lov til å legge sammen en *int* og en *float*. Den resulterende datatypen blir da *float*.
* Addisjon, subtraksjon og multiplikasjon med to *int* resulterer alltid i en *int*.
* Divisjon med to *int* resulterer i en *float*.
* Hvis det er en *float* involvert i aritmetikken blir resultatet alltid en *float*.

## Aritmetikk

Vi kan utføre flere aritmetiske funksjoner enn bare addisjon:

```python
a = 10
b = 2
print(a-b)
print(a*b)
print(a/b)
```

Dette gir utskriften:

```
8
20
5.0
```

Python har også innebygde matematiske funksjoner:

```python
a = 6.6
print(round(a))
print(pow(a,2))
```

```
7
43.559999999999995
```

Den første funksjonen gjør en avrunding til nærmeste heltall, mens den andre funksjonen utfører *a opphøyd i 2*.

I tillegg finnes det et bibliotek som heter *math* og inneholder veldig mange matematiske funksjoner. Et bibliotek er en samling med ekstra funksjonalitet som vi kan velge å benytte oss av. For å bruke et bibliotek må vi importere dette i kode. Vi skriver `import math` øverst i kodefila vår:

```python
import math

a = 6.6
print(math.floor(a))
print(math.ceil(a))
```

Dette gir utskriften:

```
6
7
```

* På den første linjen importerer vi biblioteket *math*.
* Når vi skal bruke biblioteket skriver vi `math.` og så funksjonen vi ønsker å bruke.
* I math-biblioteket har vi 2 funksjoner for avrunding, en som alltid runder av nedover, *floor()* og en som alltid runder av oppover, *ceil()*.

For å finne alle funksjonene i *math*-biblioteket kan dere se her: [https://docs.python.org/3/library/math.html](https://docs.python.org/3/library/math.html)

# Boolean

Den neste datatypen vi skal se på er *bool* eller *boolean* som er enten `True` eller `False`. Denne datatypen brukes i logiske uttrykk hvor vi ønsker å finne ut om noe er *sant* eller *usant*. Vi kommer tilbake til dette når vi skal se på beslutninger, men vi skal nå se på noen enkle eksempler:

```python
a = 5 > 6
print(a)
print(type(a))
```

Gir utskriften: 

```
False
<class 'bool'>
```

Dette er fordi uttrykket `5 > 6` er **usant**, fordi 5 er ikke større enn 6. På den andre linjen skrives datatypen ut.

Vi kan bruke følgende sammenligningsoperatorer:

* < mindre enn
* > større enn
* <= mindre enn eller lik
* >= større enn eller lik
* == er lik
* != er ikke lik

Eksempler:
* `5 < 6` er `True` fordi 5 **er** mindre enn 6
* `5 > 6` er `False` fordi 5 **er ikke** større enn 6
* `5 == 5` er `True` fordi 5 **er** lik 5
* `5 == 6` er `False` fordi 5 **er ikke** lik 6
* `5 <= 6` er `True` fordi 5 **er** mindre enn eller lik 6 (mindre)
* `5 <= 5` er `True` fordi  5 **er** mindre enn eller lik 5 (lik)
* `6 >= 6` er `True` fordi  6 **er** større enn eller lik 6 (lik)
* `6 >= 5` er `True` fordi  6 **er** større enn eller lik 5 (større)

Vi kan sammeligne strenger også:
* `"hei" == "hei"` er `True` fordi strengene **er** like
* `"hei hei" == "hei"` er `False` fordi strengene **er ikke** like
* `"hei hei" != "hei"` er `True` fordi strengene **er ikke** like
* `"Arne" < "Bård"` er `True` fordi Arne **er før** Bård alfabetisk
* `"Chris" < "Bård"` er `False` fordi Chris **er ikke før** Bård alfabetisk

## boolsk aritmetikk

Vi kan også utføre aritmetikk med boolske variabler (boolsk algebra):

```python
a = True
b = False
print(a or b)
print(a and b)
print(a and not b)
```

Dette gir utskriften:

```
True
False
True
```

* `a or b` blir `True` hvis enten `a` eller `b`, eller begge er `True`
* `a and b` blir kun `True` hvis både `a` og `b` er `True`
* Når vi skriver `not b` gjør vi verdien av `b` til det motsatte av hva den er, så hvis `b` er `False` er `not b` `True` 

# Liste

Så langt har variablene vi har brukt vært tall eller strenger eller boolske verdier. Men vi kan også ha variabler som har **flere** verdier, altså en **liste**, også kalt **tabell**.

I eksempelet under oppretter vi en liste med 4 strenger og utfører flere operasjoner på den:

```python
# Opprett en liste med 4 elementer
liste = ["Fredrik", "Arne", "Karl", "Åge"]

liste.append("Tore")  # Legg til på slutten av lista
print(liste)

liste.remove("Tore")  # Ta bort elementet "Tore"
print(liste)

liste.pop(2)  # Ta bort elementet på posisjon 2 (første posisjon er 0)
print(liste)

liste.insert(1, "Bendik")   # Legg til "Bendik" på posisjon 1
print(liste)

liste.sort()  # Sorter listen
print(liste)

liste.reverse()  # Snu listen opp ned
print(liste)

antall = len(liste)  # Finn antall elementer i listen
print(antall)
```
```
['Fredrik', 'Arne', 'Karl', 'Åge', 'Tore']
['Fredrik', 'Arne', 'Karl', 'Åge']
['Fredrik', 'Arne', 'Åge']
['Fredrik', 'Bendik', 'Arne', 'Åge']
['Arne', 'Bendik', 'Fredrik', 'Åge']
['Åge', 'Fredrik', 'Bendik', 'Arne']
4
```

Operasjonene vi utfører er:
* **liste.append(element)** - legger til et element på slutten ac lista
* **liste.remove(element)** - sletter elementet gitt i argumentet
* **liste.pop(posisjon)** - sletter elementet på gitt posisjon. Merk at første posisjon i lista er **0**
* **liste.insert(posisjon, element)** - setter inn et nytt element på gitt posisjon. Elementene på høyere posisjoner blir forskjøvet.
* **liste.sort()** - sorter lista
* **liste.reverse()** - reverser rekkefølgen i lista
* **len(liste)** - gir antall elementer i lista

Vi kan lage lister med tall istedetfor strenger:

```python
liste = [1, 67 , 18, 5]
```

Vi kan aksessere enkeltelementer i en liste på denne måten:

```python
liste = [1, 3, 5]

sum = liste[0] + liste[1] + liste[2]
print(sum)
```
```
9
```

Første element har alltid indeks 0.


Vi kan også opprette en helt tom liste og bygge den opp med brukerinput:

```python
liste = []
tall = input("Skriv inn et tall: ")
liste.append(int(tall))
tall = input("Skriv inn et tall: ")
liste.append(int(tall))
print(liste)
```

Merk at vi her konverterer input-strengen til et tall før vi legger den i lista. Dette kan være greit hvis vi skal gjøre aritmetiske operasjoner på elementer i lista senere.

Selv om det vanligste er å lage lister hvor alle elementene har samme datatype, er det også mulig å ha lister hvor hvert element har forskjellige datatyper:

```python
liste = ["Karl", "Karlsen" , 18, True]
```

Merk at hvis lista har forskjellige datatyper, så kan vi ikke sortere lista siden man ikke kan sammenligne strenger med tall.


# Oppgaver

## Oppgave 1

Lag et program som lar brukeren skrive inn tre strenger som legges i hver sin variabel. Deretter skal programmet slå sammen strengene til en og så skal strengen skrives ut, med store bokstaver og utropstegn bak!

## Oppgave 2

Lag et program som lar brukeren skrive inn fire tall. Hvert av disse tallene skal legges inn i en liste. Når brukeren har skrevet inn tallene så skal følgende skrives ut:

* Tallene som ble skrevet inn, i sortert rekkefølge.
* Summen av tallene
* Gjennomsnittet av tallene

Hvis tallene brukeren skriver inn er [12, 4, 8, 3], så skal utskriften bli:

```
Tall: 3,4,8,12
Sum: 27
Gjennomsnitt: 6.75
```

